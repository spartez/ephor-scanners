package com.spartez.assettracker.checkin.network;

public interface NetworkFragmentWatcher {
    void fragmentAttached(NetworkFragment fragment);
}
