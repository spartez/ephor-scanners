import XCTest
@testable import LabelScanner

class LabelScannerTests: XCTestCase {
    //MARK: Meal Class Tests
    func testProfileInitializationSucceeds() {
        let profile = Profile.init(url: "a", user: "a", password: "", dateFieldType: false, fieldName: "", operationId: 1, operationName: "aa")
        XCTAssertNotNil(profile)
    }
    
    func testProfileInitializationFails() {
        // Empty String
        let emptyStringProfile = Profile.init(url: "", user: "", password: "", dateFieldType: true, fieldName: "", operationId: 1, operationName: "aa")
        XCTAssertNil(emptyStringProfile)
    }
}
