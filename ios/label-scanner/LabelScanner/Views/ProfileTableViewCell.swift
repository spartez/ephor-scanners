import UIKit
import os.log

class ProfileTableViewCell: UITableViewCell {

    //MARK: Properties
    @IBOutlet weak var fieldOrOperation: UILabel!
    @IBOutlet weak var url: UILabel!
    @IBOutlet weak var typeImage: UIImageView!
    @IBOutlet weak var editPadding: NSLayoutConstraint!
    
    var editMarker = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        if (!isEditing) {
            editMarker = false
            editPadding.constant = 10
        } else if (isEditing && !showingDeleteConfirmation) {
            editMarker = true
            editPadding.constant = 10
        } else if (isEditing && showingDeleteConfirmation) {
            editPadding.constant = editMarker ? 110 : 145
            editMarker = false
        }
        
        super.layoutSubviews()
        
        let separatorLineView = UIView(frame: CGRect(x: 10, y: bounds.size.height - 0.5, width: bounds.size.width + 200, height: 0.5))
        separatorLineView.backgroundColor = UIColor.lightGray
        addSubview(separatorLineView)
    }

}
