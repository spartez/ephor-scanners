import UIKit
import AVFoundation
import os.log

class QrCodeScannerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    //MARK: Properties
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var torchButton: UIButton!
    
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var qrCodeFrameView:UIView?
    
    var lastScannedValue:String? = nil
    var lastError:String? = nil
    
    let supportedBarCodes = [AVMetadataObjectTypeQRCode]
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            captureSession = AVCaptureSession()
            captureSession?.addInput(input)
            
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput)
            
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            
            captureMetadataOutput.metadataObjectTypes = supportedBarCodes
            
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
            videoPreviewLayer?.frame = view.layer.bounds
            view.layer.addSublayer(videoPreviewLayer!)
            
            captureSession?.startRunning()
            
            // Move the label and button to the top view
            view.bringSubview(toFront: messageLabel)
            view.bringSubview(toFront: toolbar)
            view.bringSubview(toFront: torchButton)
            
            // Initialize QR Code Frame to highlight the QR code
            qrCodeFrameView = UIView()
            
            if let qrCodeFrameView = qrCodeFrameView {
                qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
                qrCodeFrameView.layer.borderWidth = 2
                view.addSubview(qrCodeFrameView)
                view.bringSubview(toFront: qrCodeFrameView)
            }
            
            if isTorchOn() {
                torchButton.setImage(UIImage(named: "notorch"), for: .normal)
            } else {
                torchButton.setImage(UIImage(named: "torch"), for: .normal)
            }
        } catch {
            lastError = error.localizedDescription
            if let session = captureSession, session.isRunning {
                session.stopRunning()
            }
            performSegue(withIdentifier: "ScanError", sender: self)
//            self.dismiss(animated: false, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects == nil || metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            messageLabel.text = "Scan QR code"
            lastScannedValue = nil
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        // Here we use filter method to check if the type of metadataObj is supported
        // Instead of hardcoding the AVMetadataObjectTypeQRCode, we check if the type
        // can be found in the array of supported bar codes.
        if supportedBarCodes.contains(metadataObj.type) {
            //        if metadataObj.type == AVMetadataObjectTypeQRCode {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            if metadataObj.stringValue != nil {
                messageLabel.text = metadataObj.stringValue
                
                if lastScannedValue != metadataObj.stringValue {
                    // shutter
                    if Settings.get().shutterSoundOn() {
                        AudioServicesPlaySystemSound(1108)
                    }
                    lastScannedValue = metadataObj.stringValue
                    captureSession?.stopRunning()
                    performSegue(withIdentifier: "ScanCompleted", sender: self)
//                    self.dismiss(animated: false, completion: nil)
                }
            }
        } else {
            lastScannedValue = nil
        }
    }

    //MARK: - Actions
    @IBAction func stop(_ sender: Any) {
        performSegue(withIdentifier: "ScanCancelled", sender: self)
    }
    
    @IBAction func toggleTorch(_ sender: Any) {
        guard let device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo) else {
            Log.error("No camera found")
            return
        }
        
        if (device.hasTorch) {
            do {
                try device.lockForConfiguration()
                if (device.torchMode == AVCaptureTorchMode.on) {
                    device.torchMode = AVCaptureTorchMode.off
                    torchButton.setImage(UIImage(named: "torch"), for: .normal)
                } else {
                    do {
                        try device.setTorchModeOnWithLevel(1.0)
                        torchButton.setImage(UIImage(named: "notorch"), for: .normal)
                    } catch {
                        Log.error("Error toggling torch: \(error.localizedDescription)")
                    }
                }
                device.unlockForConfiguration()
            } catch {
                Log.error("Error toggling torch: \(error.localizedDescription)")
            }
        }
    }
    
    private func isTorchOn() -> Bool {
        guard let device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo) else {
            Log.error("No camera found")
            return false
        }
        
        if (device.hasTorch) {
            do {
                return device.torchMode == AVCaptureTorchMode.on
            } catch {
                Log.error("Error querying torch: \(error.localizedDescription)")
            }
        }
        return false
    }
}
