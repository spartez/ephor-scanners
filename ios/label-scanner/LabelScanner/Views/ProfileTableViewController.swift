import UIKit
import BGTableViewRowActionWithImage
import Toast_Swift
import KeenClient

class ProfileTableViewController: UITableViewController {
    
    //MARK: Properties
    @IBOutlet weak var emptyTableView: UITextView!
    
    var profiles = [Profile]()

    private var throbber = Throbber()
    private var analytics = Analytics()
    
    static var wasNotEmpty = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        navigationItem.leftBarButtonItem = editButtonItem

        if let savedProfiles = loadProfiles() {
            profiles += savedProfiles
            if profiles.count > 0 {
                ProfileTableViewController.wasNotEmpty = true
            }
        }
        
        analytics.trackAppStart(profilesNumber: profiles.count)
        
        emptyTableView.textContainerInset = UIEdgeInsets(top: 20, left: 10, bottom: 20, right: 10)
        setTableFooterText()
    }

    override func viewDidAppear(_ animated: Bool) {
        throbber.hide()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profiles.count
    }

    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let scanAction = BGTableViewRowActionWithImage.rowAction(with: .default, title: "Scan", titleColor: .black, backgroundColor: .green, image: UIImage(named: "qrcode"), forCellHeight: UInt(tableView.rowHeight), handler: { action, indexpath in
            self.performSegue(withIdentifier: "ScanWithProfile", sender: indexPath)
        })
        
        let moreAction = BGTableViewRowActionWithImage.rowAction(with: .default, title: "More", titleColor: .black, backgroundColor: .lightGray, image: UIImage(named: "menu"), forCellHeight: UInt(tableView.rowHeight), handler: { action, indexpath in
            self.showProfileMoreMenu(indexPath: indexPath)
        })
        return [ moreAction!, scanAction! ]
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "ProfileTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ProfileTableViewCell  else {
            fatalError("The dequeued cell is not an instance of ProfileTableViewCell.")
        }
        
        // Fetches the appropriate profile for the data source layout.
        let profile = profiles[indexPath.row]
        
        cell.url.text = profile.url
        cell.fieldOrOperation.text = profile.dateFieldType ? profile.fieldName : profile.operationName
        cell.typeImage.image = UIImage(named: profile.dateFieldType ? "date" : "play")

        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            profiles.remove(at: indexPath.row)
            saveProfiles()
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }

    /*
    override func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath) {
    }
    
    override func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?) {
    }
    */
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        if editing {
            UIView.setAnimationsEnabled(false)
        } else {
            UIView.setAnimationsEnabled(true)
        }
        super.setEditing(editing, animated: animated)        
    }
    
    //MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        switch(segue.identifier ?? "") {
        case "AddProfile":
//            Log.debug("Adding a new profile.")
            throbber.show(parent: view)
        case "ShowAbout":
            Log.debug("Showing About dialog.")
        case "ScanWithProfile":
            guard let scanLogController = segue.destination.childViewControllers[0] as? QrCodeScannerLogViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            guard let indexPath = sender as? IndexPath else {
                fatalError("The selected cell is not being displayed by the table")
            }
            scanLogController.profile = profiles[indexPath.row]
        case "CloneProfile":
            guard let profileDetailViewController = segue.destination as? ProfileViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            guard let profile = sender as? Profile else {
                fatalError("Unexpected sender: \(sender)")
            }
            profileDetailViewController.cloning = true
            profileDetailViewController.profile = profile
        case "ShowProfileDetail":
            guard let profileDetailViewController = segue.destination as? ProfileViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            guard let selectedProfileCell = sender as? ProfileTableViewCell else {
                fatalError("Unexpected sender: \(sender)")
            }
            
            guard let indexPath = tableView.indexPath(for: selectedProfileCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            
            let selectedProfile = profiles[indexPath.row]
            profileDetailViewController.profile = selectedProfile
        default:
            fatalError("Unexpected Segue Identifier; \(segue.identifier)")
        }
    }

    //MARK: Actions
    
    @IBAction func showHelp(_ sender: Any) {
        let url = NSURL(string: "https://confluence.spartez.com/display/AT4J/Invoking+operation+sequences+by+scanning+asset+label")! as URL
        UIApplication.shared.openURL(url)
    }
    
    @IBAction func showSettings(_ sender: Any) {
        UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
    }

    @IBAction func unwindAddedOrUpdatedToProfileList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? ProfileViewController, let profile = sourceViewController.profile {
            if sourceViewController.cloning ?? false {
                let newIndexPath = IndexPath(row: profiles.count, section: 0)
                profiles.append(profile)
                tableView.insertRows(at: [newIndexPath], with: .automatic)
            } else {
                if let selectedIndexPath = tableView.indexPathForSelectedRow {
                    profiles[selectedIndexPath.row] = profile
                    tableView.reloadRows(at: [selectedIndexPath], with: .none)
                }
            }
            saveProfiles()
        } else if let sourceViewController = sender.source as? NewProfileTypeViewController, let profile = sourceViewController.profile {
            
            let newIndexPath = IndexPath(row: profiles.count, section: 0)
            profiles.append(profile)
            tableView.insertRows(at: [newIndexPath], with: .automatic)
            
            saveProfiles()
        }
    }
    
    //MARK: Private Methods
    
    private func showProfileMoreMenu(indexPath: IndexPath) {
        let menu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let deleteAction = UIAlertAction(title: "Delete", style: .destructive, handler: {(alert: UIAlertAction!) -> Void in
            self.profiles.remove(at: indexPath.row)
            self.saveProfiles()
            self.tableView.deleteRows(at: [indexPath], with: .fade)
        })
        let cloneAction = UIAlertAction(title: "Copy", style: .default, handler: {(alert: UIAlertAction!) -> Void in
            self.cloneProfile(profile: self.profiles[indexPath.row])
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) -> Void in
        })
        menu.addAction(deleteAction)
        menu.addAction(cloneAction)
        menu.addAction(cancelAction)
        
        present(menu, animated: true, completion: nil)
    }
    
    private func cloneProfile(profile: Profile) {
        performSegue(withIdentifier: "CloneProfile", sender: profile.copy() as! Profile)
    }
    
    private func saveProfiles() {
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(profiles, toFile: Profile.ArchiveURL.path)
        
        if isSuccessfulSave {
            Log.debug("Profiles successfully saved.")
        } else {
            Log.error("Failed to save profiles.")
        }
        setTableFooterText()
        ProfileTableViewController.wasNotEmpty = true
    }
    
    private func loadProfiles() -> [Profile]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: Profile.ArchiveURL.path) as? [Profile]
    }
    
    private func setTableFooterText() {
        var text: String
        if (profiles.count > 0) {
            text = "<div style=\"color:gray; font-style: italic; font-size: smaller\">Swipe left for operations</div>"
        } else {
            text =
                "<p>" +
                "Scan profiles determine what happens when you scan an asset label." +
                " It can either be a predefined operation sequence or a date field update." +
                "</p>" +
                "<p>" +
                "You have not defined any profiles.<br/> Click the \"+\" above to add one." +
                "</p>"
        }
        do {
            let textWithFont = NSString(format:"<div style=\"font-family: '-apple-system', 'HelveticaNeue'; font-size: 17\">%@</div>" as NSString, text) as String
            let attrStr = try NSAttributedString(
                data: textWithFont.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                documentAttributes: nil
            )
            emptyTableView.attributedText = attrStr
        } catch  {
            Log.error("Error setting label: %s", args: error.localizedDescription)
        }
        if profiles.count > 0 {
            emptyTableView.textContainerInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 10)
        } else {
            // stupid layout quirks. Empty table does not seem to have height and shows footer 
            // not laid out properly. Table that used to not be empty but now is *has* a height 
            // and displays footer in the "right" place
            emptyTableView.textContainerInset = UIEdgeInsets(top: ProfileTableViewController.wasNotEmpty ? 10 : 80, left: 10, bottom: 0, right: 10)
        }
        emptyTableView.sizeToFit()
    }
}
