import UIKit

class QrCodeScannerLogViewController: UIViewController {

    //MARK: Properties
    @IBOutlet weak var scanLog: UITextView!
    @IBOutlet weak var scanButton: UIBarButtonItem!
    
    var logEntries = [String]()
    
    var profile: Profile?
    var stateMachine: AssetOperationStateMachine?
    var throbber = Throbber()
    
    class NotImplementedOperation : AssetOperationPhase {
        var name = "start"
        
        var parent: QrCodeScannerLogViewController
        
        init(_ parent: QrCodeScannerLogViewController) {
            self.parent = parent
        }
        
        func run(
            currentPhase: String,
            state: AssetOperationState,
            onSuccess: @escaping (AssetOperationPhase, AssetOperationState) -> Void,
            onError: @escaping (AssetOperationPhase, AssetOperationState) -> Void
        ) {
            let alert = UIAlertController(
                title: "Not implemented",
                message: "Not implemented yet, kick me",
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "Close", style: .cancel) { action in
                state.errorMessage = "not implemented"
                onError(self, state)
            })
            let when = DispatchTime.now() + 1
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.parent.present(alert, animated: true)
            }
        }
        
        func getOnSuccessPhaseName() -> String {
            return "error"
        }
        
        func getOnErrorPhaseName() -> String {
            return "error"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setLogText(text: "<br/><center>Press button below to scan QR code</center>")
        
        scanLog.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        guard profile != nil else {
            fatalError("Profile not set for scanning")
        }
        stateMachine = AssetOperationStateMachine(
            profile: profile!,
            phases: profile!.dateFieldType
                ? [
                    GetAsset("start", successPhase: "getAssetType", errorPhase: "error"),
                    GetAssetType("getAssetType", haveFieldPhase: "updateDateField", dontHaveFieldPhase: "maybeAddDateField", errorPhase: "error"),
                    MaybeAddDateField("maybeAddDateField", parent: self, yesPhase: "getDateField", noPhase: "error"),
                    GetDateField("getDateField", successPhase: "addDateField", errorPhase: "error"),
                    AddDateField("addDateField", parent: self, successPhase: "updateDateField", errorPhase: "error"),
                    UpdateDateField("updateDateField", successPhase: "end", errorPhase: "error")
                ]
                : [ RunStoredOperation("start", successPhase: "end", errorPhase: "error") ]
        )
        guard stateMachine != nil else {
            fatalError("Unable to crate asset update state machine")
        }
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(QrCodeScannerLogViewController.redrawThrobber),
            name: NSNotification.Name.UIDeviceOrientationDidChange,
            object: nil
        )
        
        throbber.show(parent: view)
        self.performSegue(withIdentifier: "DoScan", sender: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func unwindScanned(sender: UIStoryboardSegue) {
        if let scanner = sender.source as? QrCodeScannerViewController {
//            Log.debug("Scanned QR code")
            throbber.hide()
            if let value = scanner.lastScannedValue {
                let asset = Asset(url: value)
                if asset == nil {
                    logEntries.append("Scanned <a href=\"" + value + "\">unknown QR code</a>")
                    setLogText(text: logEntries.joined(separator: "<br/>"))
                } else {
                    let aUrl = asset?.url ?? ""
                    let pUrl = profile?.url ?? "invalid"
                    if Settings.get().forceJiraUrl() && !aUrl.hasPrefix(pUrl) {
                        logEntries.append("JIRA URL in the <a href=\"\(aUrl)\">QR code</a> does not match URL in the profile: <a href=\"\(pUrl)\">\(pUrl)</a>")
                        setLogText(text: logEntries.joined(separator: "<br/>"))
                    } else {
                        runUpdate(asset: asset!)
                    }
                }
            }
        }
    }

    @IBAction func unwindScanError(sender: UIStoryboardSegue) {
        if let scanner = sender.source as? QrCodeScannerViewController {
            Log.debug("Error scanning QR code")
            if let value = scanner.lastError {
                logEntries.append(value)
                setLogText(text: logEntries.joined(separator: "<br/>"))
            }
        }
    }

    @IBAction func unwindCancelScan(sender: UIStoryboardSegue) {
        throbber.hide()
    }

    //MARK: Private methods
    
    private func runUpdate(asset: Asset) {
        scanButton.isEnabled = false
        throbber.show(parent: view)
        
        stateMachine!.run(
            asset: asset,
            onUpdateSuccess: { state in
                let entry = "Updated asset <a href=\"\(asset.url)\">#\(asset.id)</a>"
                self.logEntries.append(entry)
                self.setLogText(text: self.logEntries.joined(separator: "<br/>"))
                self.scanButton.isEnabled = true
                if Settings.get().scanAutoStart() {
                    self.throbber.show(parent: self.view)
                    self.performSegue(withIdentifier: "DoScan", sender: self)
                } else {
                    self.throbber.hide()
                }
            },
            onUpdateError: { state in
                let entry = "Error updating asset <a href=\"\(asset.url)\">#\(asset.id)</a>: \(state.errorMessage ?? "Unspecified error")"
                self.logEntries.append(entry)
                self.setLogText(text: self.logEntries.joined(separator: "<br/>"))
                self.scanButton.isEnabled = true
                self.throbber.hide()
            }
        )
    }
    
    private func setLogText(text: String) {
        let textWithFont = NSString(format: "<div style=\"font-family: '-apple-system', 'HelveticaNeue'; font-size: 17\">%@</div>" as NSString, text) as String
        do {
            let attrStr = try NSAttributedString(
                data: textWithFont.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                documentAttributes: nil
            )
            scanLog.attributedText = attrStr
            
            let when = DispatchTime.now() + 0.5
            DispatchQueue.main.asyncAfter(deadline: when) {
                if self.scanLog.contentSize.height + 80 > self.scanLog.bounds.size.height {
                    let point = CGPoint(x: 0, y: 40 + self.scanLog.contentSize.height - self.scanLog.bounds.size.height)
                    self.scanLog.setContentOffset(point, animated: true)
                }
            }

            redrawThrobber()
        } catch  {
            Log.error("Error adding log: %s", args: error.localizedDescription)
        }
    }
    
    @objc private func redrawThrobber() {
        if self.throbber.visible {
            self.throbber.show(parent: self.view)
        }
    }
}
