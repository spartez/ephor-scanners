import UIKit
import Toast_Swift
import ActionSheetPicker_3_0
import Alamofire
import SwiftyJSON

class ProfileViewController: UIViewController, UITextFieldDelegate, UINavigationControllerDelegate, TouchObserver {

    //MARK: Properties
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var testButton: UIBarButtonItem!
    @IBOutlet weak var scanButton: UIBarButtonItem!
    @IBOutlet weak var urlField: UITextField!
    @IBOutlet weak var userNameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var fieldOrOperationPickerLabel: UILabel!
    @IBOutlet weak var fieldOrOperationField: TextFieldWithThrobber!

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var wrapperView: TouchableView!
    @IBOutlet weak var image: UIImageView!
    
    var profile: Profile?
    var dateFieldNewProfile: Bool?
    var cloning: Bool?
    
    private var throbber = Throbber()
    
    class FieldOrOperation {
        var id: UInt
        var name: String
        
        init(id: UInt, name: String) {
            self.id = id
            self.name = name
        }
    }
    
    var fieldOrOperationList = [FieldOrOperation]()
    var selectedIndex = -1
    
    var network = Network()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        urlField.delegate = self
        userNameField.delegate = self
        passwordField.delegate = self
        fieldOrOperationField.delegate = self
        
        
        urlField.setBottomBorder()
        userNameField.setBottomBorder()
        passwordField.setBottomBorder()
        fieldOrOperationField.setBottomBorder()
        
        if let profile = profile {
            navigationItem.title = cloning ?? false ? "Copy Profile" : "Edit Profile"
            fieldOrOperationPickerLabel.text = profile.dateFieldType ? "Date Field" : "Operation Sequence"
            fieldOrOperationField.placeholder = profile.dateFieldType ? "Select date field" : "Select sequence"
            fieldOrOperationField.text = profile.dateFieldType ? profile.fieldName : profile.operationName
            urlField.text = profile.url
            userNameField.text = profile.user
            passwordField.text = profile.password
            
            image.image = UIImage(named: profile.dateFieldType ? "date" : "play")
            
            if profile.dateFieldType {
                loadDateFields(andShowPicker: false)
            } else {
                loadStoredOps(andShowPicker: false)
            }
        } else {
            navigationItem.title = "Add Profile"
            if let dateField = dateFieldNewProfile {
                selectedIndex = -1
                fieldOrOperationPickerLabel.text = dateField ? "Date Field" : "Operation Sequence"
                fieldOrOperationField.placeholder = dateField ? "Select date field" : "Select sequence"
                image.image = UIImage(named: dateField ? "date" : "play")
            }
        }
        
        updateSaveButtonState()
        
        wrapperView.onTouch = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileViewController.rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if profile == nil {
            urlField.becomeFirstResponder()
        }
    }
    
    func touchStarted() {
        view.endEditing(true)
    }
    
    func touchEnded() {
    }
    
    //MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == urlField {
            prependHttps()
            userNameField.becomeFirstResponder()
        } else if textField == userNameField {
            passwordField.becomeFirstResponder()
        } else if textField == passwordField {
            fieldOrOperationField.becomeFirstResponder()
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateSaveButtonState()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == fieldOrOperationField {
            fieldOrOperationField.endEditing(true)
            fieldOrOperationField.resignFirstResponder()
            urlField.endEditing(true)
            urlField.resignFirstResponder()
            userNameField.endEditing(true)
            userNameField.resignFirstResponder()
            passwordField.endEditing(true)
            passwordField.resignFirstResponder()
            if (profile != nil) {
                if profile!.dateFieldType {
                    loadDateFields(andShowPicker: true)
                } else {
                    loadStoredOps(andShowPicker: true)
                }
            } else {
                if dateFieldNewProfile ?? true {
                    loadDateFields(andShowPicker: true)
                } else {
                    loadStoredOps(andShowPicker: true)
                }
            }
        }
    }
    
    //MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if segue.identifier == "Scan" {
            guard let scanView = segue.destination.childViewControllers[0] as? QrCodeScannerLogViewController else {
                fatalError("Invalid destination for \"Scan\" segue")
            }
            scanView.profile = getProfile()
        } else {
            guard let button = sender as? UIBarButtonItem, button === saveButton else {
                Log.debug("The save button was not pressed, cancelling")
                return
            }
            profile = getProfile()
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        // Depending on style of presentation (modal or push presentation), this view controller needs to be dismissed in two different ways.
        let isPresentingInAddProfileMode = presentingViewController is UINavigationController
        
        if isPresentingInAddProfileMode {
            performSegue(withIdentifier: "CancelAdding", sender: sender)
        } else if let owningNavigationController = navigationController {
            owningNavigationController.popViewController(animated: true)
        } else {
            fatalError("The ProfileViewController is not inside a navigation controller.")
        }
    }
    
    @IBAction func save(_ sender: Any) {
        let isPresentingInAddProfileMode = presentingViewController is UINavigationController
        
        if isPresentingInAddProfileMode {
            performSegue(withIdentifier: "Add", sender: sender)
        } else {
            performSegue(withIdentifier: "Update", sender: sender)
        }
    }
    
    var testCalls = [TestCall]()
    var testCallIndex = 0
    var testCallResult = [String]()
    
    @IBAction func testConnection(_ sender: Any) {
        throbber.show(parent: view)
        testButton.isEnabled = false
        
        testCalls.removeAll()
        testCallResult.removeAll()
        testCalls.append(GetServerInfo())
        testCalls.append(GetFolders())
        
        let dateFieldType = profile != nil ? profile!.dateFieldType : (dateFieldNewProfile ?? true)
        if dateFieldType {
            testCalls.append(GetFieldTypes())
        } else {
            testCalls.append(GetStoredOperations())
        }
        testCallIndex = -1
        nextTestCall()
    }
    
    //MARK: Private Methods
    
    private class GetServerInfo : TestCall {
        var url = "/rest/api/2/serverInfo"
        var describe = "JIRA server info"
        func from(result: JSON) -> String {
            do {
                return "v" + (result.dictionary!["version"]?.stringValue)! + " " + (result.dictionary!["serverTitle"]?.stringValue)!
            } catch {
                return "Error"
            }
        }
    }

    private class GetFolders : TestCall {
        var url = "/rest/com-spartez-ephor/1.0/category"
        var describe = "Asset Tracker folders"
        func from(result: JSON) -> String {
            do {
                return String(result.dictionary!["subcategories"]!.array!.count) + " toplevel folders"
            } catch {
                return "Error"
            }
        }
    }

    private class GetStoredOperations : TestCall {
        var url = "/rest/com-spartez-ephor/1.0/stored-operation"
        var describe = "Operation sequences"
        func from(result: JSON) -> String {
            do {
                return String(result.array!.count) + " sequences"
            } catch {
                return "Error"
            }
        }
    }

    private class GetFieldTypes : TestCall {
        var url = "/rest/com-spartez-ephor/1.0/fieldtype"
        var describe = "Asset Tracker fields"
        func from(result: JSON) -> String {
            do {
                return String(result.array!.count) + " field types"
            } catch {
                return "Error"
            }
        }
    }

    private func nextTestCall() {
        testCallIndex += 1
        if testCallIndex >= testCalls.count {
            displayTestResults()
            return
        }
        let testCall = testCalls[testCallIndex]
        let url = "\(urlField.text!)\(testCall.url).json?_=\(Date().getTime())"
        network.get(url, user: userNameField.text!, password: passwordField.text!,
            onSuccess: { optJson in
                guard let json = optJson else {
                    self.testCallResult.append(testCall.describe + ": empty response")
                    self.displayTestResults()
                    return
                }

                let result = testCall.from(result: json)
                self.testCallResult.append(testCall.describe + ": " + result)
                self.nextTestCall()
            },
            onError: { error in
                self.testCallResult.append(testCall.describe + ": " + error)
                self.displayTestResults()
            }
        )
    }
    
    private func displayTestResults() {
        throbber.hide()
        testButton.isEnabled = true
        let text = testCallResult.joined(separator: "\n")
        self.view.makeToast(text, duration: 3.0, position: .bottom, title: "Connection test result", image: nil, style: nil, completion: nil)
    }
    
    private func updateSaveButtonState() {
        let url = urlField.text ?? ""
        let user = userNameField.text ?? ""
        saveButton.isEnabled = !url.isEmpty && !user.isEmpty && selectedIndex >= 0
        scanButton.isEnabled = saveButton.isEnabled
    }
    
    private func prependHttps() {
        let text = urlField.text ?? ""
        if !text.hasPrefix("http://") && !text.hasPrefix("https://") {
            urlField.text = "https://" + text
        }
    }
    
    private func loadStoredOps(andShowPicker: Bool) {
        fieldOrOperationList.removeAll()
        let url =  "\(urlField.text!)/rest/com-spartez-ephor/1.0/stored-operation.json?_=\(Date().getTime())"
        
        fieldOrOperationField.showThrobber()
        testButton.isEnabled = false
        
        network.get(url, user: userNameField.text!, password: passwordField.text!,
            onSuccess: { optJson in
                guard let json = optJson else {
                    self.view.makeToast("Error retrieving operation sequences: empty response", duration: 3.0, position: .bottom, title: "Error", image: nil, style: nil, completion: nil)
                    self.fieldOrOperationField.hideThrobber()
                    self.testButton.isEnabled = true
                    return
                }

                guard let array = json.array else {
                    self.view.makeToast("Error retrieving operation sequences: unexpected response", duration: 3.0, position: .bottom, title: "Error", image: nil, style: nil, completion: nil)
                    self.fieldOrOperationField.hideThrobber()
                    self.testButton.isEnabled = true
                    return
                }
                if array.count > 0 {
                    for index in 0...(array.count - 1) {
                        var field = array[index].dictionary!
                        self.fieldOrOperationList.append(FieldOrOperation(id: (field["id"]?.uInt)!, name: (field["name"]?.stringValue)!))
                    }
                }
                self.selectedIndex = -1
                if self.fieldOrOperationList.count == 0 {
                    self.updateSaveButtonState()
                    let alert = UIAlertController(
                        title: "No sequences found",
                        message: "No operation sequences are defined in Asset Tracker. Add one and try again.",
                        preferredStyle: .alert
                    )
                    alert.addAction(UIAlertAction(title: "Close", style: .cancel))
                    self.present(alert, animated: true)
                } else {
                    if self.profile == nil {
                        self.selectedIndex = 0
                    } else {
                        for index in 0...(self.fieldOrOperationList.count - 1) {
                            if self.fieldOrOperationList[index].id == self.profile!.operationId {
                                self.selectedIndex = index
                                break
                            }
                        }
                    }
                    if andShowPicker {
                        self.showPicker()
                    } else {
                        self.updateSaveButtonState()
                    }
                }
                self.fieldOrOperationField.hideThrobber()
                self.testButton.isEnabled = true
            },
            onError: { errorMessage in
                self.view.makeToast("Error retrieving operation sequences: " + errorMessage, duration: 3.0, position: .bottom, title: "Error", image: nil, style: nil, completion: nil)
                self.fieldOrOperationField.hideThrobber()
                self.testButton.isEnabled = true
            }
        )
    }
    
    private func loadDateFields(andShowPicker: Bool) {
        fieldOrOperationList.removeAll()
        let url = "\(urlField.text!)/rest/com-spartez-ephor/1.0/fieldtype.json?_=\(Date().getTime())"
        
        fieldOrOperationField.showThrobber()
        testButton.isEnabled = false
       
        network.get(url, user: userNameField.text!, password: passwordField.text!,
            onSuccess: { optJson in
                guard let json = optJson else {
                    self.view.makeToast("Error retrieving date fields: empty response", duration: 3.0, position: .bottom, title: "Error", image: nil, style: nil, completion: nil)
                    self.fieldOrOperationField.hideThrobber()
                    self.testButton.isEnabled = true
                    return
                }
                guard let array = json.array else {
                    self.view.makeToast("Error retrieving date fields: unexpected response", duration: 3.0, position: .bottom, title: "Error", image: nil, style: nil, completion: nil)
                    self.fieldOrOperationField.hideThrobber()
                    self.testButton.isEnabled = true
                    return
                }
                if array.count > 0 {
                    for index in 0...(array.count - 1) {
                        var field = array[index].dictionary!
                        if field["templateName"]?.stringValue != "date" {
                            continue
                        }
                        self.fieldOrOperationList.append(FieldOrOperation(id: (field["id"]?.uInt)!, name: (field["name"]?.stringValue)!))
                    }
                }
                self.selectedIndex = -1
                if self.fieldOrOperationList.count == 0 {
                    self.updateSaveButtonState()
                    let alert = UIAlertController(
                        title: "Date fields not found",
                        message: "No fields of type \"Date\" were found in Asset Tracker\'s data scheme. Add them in the Asset Tracker settings.",
                        preferredStyle: .alert
                    )
                    alert.addAction(UIAlertAction(title: "Close", style: .cancel))
                    self.present(alert, animated: true)
                } else {
                    if self.profile == nil {
                        self.selectedIndex = 0
                    } else {
                        for index in 0...(self.fieldOrOperationList.count - 1) {
                            if self.fieldOrOperationList[index].name == self.profile!.fieldName {
                                self.selectedIndex = index
                                break
                            }
                        }
                    }
                    if andShowPicker {
                        self.showPicker()
                    } else {
                        self.updateSaveButtonState()
                    }
                }
                self.fieldOrOperationField.hideThrobber()
                self.testButton.isEnabled = true
            },
            
            onError: { errorMessage in
                self.view.makeToast("Error retrieving date fields: " + errorMessage, duration: 3.0, position: .bottom, title: "Error", image: nil, style: nil, completion: nil)
                self.fieldOrOperationField.hideThrobber()
                self.testButton.isEnabled = true
            }
        )
    }
    
    private func showPicker() {
        updateSaveButtonState()
        
        var list = [String]()
        for index in 0...(fieldOrOperationList.count - 1) {
            list.append(fieldOrOperationList[index].name)
        }
        ActionSheetStringPicker.show(
            withTitle: (fieldOrOperationField.placeholder ?? "").capitalized,
            rows: list,
            initialSelection: max(self.selectedIndex, 0),
            doneBlock: {
                picker, index, value in
                    self.fieldOrOperationField.text = self.fieldOrOperationList[index].name
                    self.selectedIndex = index
                    self.updateSaveButtonState()
                    return
            },
            cancel: {
                ActionStringCancelBlock in return
            },
            origin: fieldOrOperationField
        )
    }
    
    @objc private func rotated() {
        var height: CGFloat
        if UIDevice.current.orientation.isLandscape {
            height = 600
        } else {
            height = wrapperView.frame.size.height + 40
        }
        
        scrollView.contentSize = CGSize(width: wrapperView.frame.size.width, height: height)
        if throbber.visible {
            throbber.show(parent: view)
        }
    }
    
    private func getProfile() -> Profile? {
        let dateFieldType = profile != nil ? profile!.dateFieldType : (dateFieldNewProfile ?? true)

        let url = urlField.text ?? ""
        let user = userNameField.text ?? ""
        let password = passwordField.text ?? ""
        let fieldName = dateFieldType ? fieldOrOperationList[selectedIndex].name : ""
        let operationId = fieldOrOperationList[selectedIndex].id
        let operationName = dateFieldType ? "" : fieldOrOperationList[selectedIndex].name
        
        return Profile(
            url: url,
            user: user,
            password: password,
            dateFieldType: dateFieldType,
            fieldName: fieldName,
            operationId: operationId,
            operationName: operationName
        )
    }
}

protocol TestCall {
    var url: String { get }
    var describe: String { get }
    func from(result: JSON) -> String
}



