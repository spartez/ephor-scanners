import UIKit

class AboutViewController: UIViewController {

    //MARK: - Properties
    @IBOutlet weak var descriptionText: UITextView!
    @IBOutlet weak var wrapperView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        
        setTextViewText(field: descriptionText, text:
            "<center>Version " + version + ", &copy; 2017 <a href=\"http://spartez.com\">Spartez</a></center><br/>" +
            "<p>" +
                "A helper application for <a href=\"https://spartez.com/products/asset-tracker-for-jira\">Asset Tracker for JIRA</a> from Spartez." +
                " It is used to scan QR code labels printed from Asset Tracker and placed on assets." +
                "</p>" +
                "<p>" +
                "Scanning the label causes the selected operation sequence to be executed on an asset record," +
                " or the selected date field of the asset record to be set to the current date." +
                " It can be used to update assets automatically by scanning their labels and" +
                " to perform periodic asset checks to find which assets are present and accounted for." +
            "</p>" +
            "<center style=\"font-size: smaller\"><br/>Licensed under <a href=\"http://www.apache.org/licenses/LICENSE-2.0\">Apache License</a><br/>" +
                "<a href=\"https://bitbucket.org/spartez/ephor-scanners\">Source code</a></center>"
            , fontSize: 17)

        descriptionText.sizeToFit()
        
        NotificationCenter.default.addObserver(self, selector: #selector(AboutViewController.rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        rotated()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    //MARK: - Actions
    
    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    //MARK: Private Methods

    private func setTextViewText(field: UITextView, text: String, fontSize: UInt) {
        let textWithFont = NSString(format:"<div style=\"font-family: '-apple-system', 'HelveticaNeue'; font-size: \(fontSize)\">%@</div>" as NSString, text) as String
        do {
            let attrStr = try NSAttributedString(
                data: textWithFont.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                documentAttributes: nil
            )
            field.attributedText = attrStr
        } catch  {
            Log.error("Error setting label: %s", args: error.localizedDescription)
        }
    }
    
    @objc private func rotated() {
        var height: CGFloat
        if UIDevice.current.orientation.isLandscape {
            height = 600
        } else {
            height = 800
        }
        
        descriptionText.sizeToFit()
        scrollView.contentSize = CGSize(width: wrapperView.frame.size.width, height: height)
    }
}
