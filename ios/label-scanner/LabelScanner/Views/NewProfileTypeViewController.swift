import UIKit

class NewProfileTypeViewController: UIViewController {

    var profile: Profile?
    
    @IBOutlet weak var storedOpsDescription: UITextView!
    @IBOutlet weak var dateFieldDescription: UITextView!
    
    let storedOpsDescriptionText =
        "Scanning a label using this profile automatically updates the asset record by performing an " +
        "<a href=\"https://confluence.spartez.com/display/AT4J/Invoking+operation+sequences+by+scanning+asset+label\">operation sequence</a>."
    let dateFieldDescriptionText =
        "Scanning a label using this profile automatically sets the value of a selected date field to the current date and time. " +
        "You can then perform Asset Tracker query for assets which don't have this field set to the present date, thus " +
        "<a href=\"https://confluence.spartez.com/display/AT4J/How+to+perform+inventory+checks\">locating missing assets</a>."
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTextViewText(field: storedOpsDescription, text: storedOpsDescriptionText)
        setTextViewText(field: dateFieldDescription, text: dateFieldDescriptionText)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    //MARK: - Navigation
    @IBAction func cancel(_ sender: Any) {
        profile = nil
        dismiss(animated: true, completion: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        switch(segue.identifier ?? "") {
        case "AddDateProfile":
            let profileDetailViewController = getProfileViewController(segue: segue)
            profileDetailViewController.dateFieldNewProfile = true
        case "AddStoredOperationProfile":
            let profileDetailViewController = getProfileViewController(segue: segue)
            profileDetailViewController.dateFieldNewProfile = false
        case "SaveNewProfile":
            Log.debug("Saving a new profile.")
        default:
            fatalError("Unexpected Segue Identifier; \(segue.identifier)")
        }
    }

    //MARK: Actions
    
    @IBAction func unwindAddedToProfileList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? ProfileViewController {
            profile = sourceViewController.profile
        }
        self.performSegue(withIdentifier: "SaveNewProfile", sender: sender)
    }

    @IBAction func dismissFromAdding(sender: UIStoryboardSegue) {
        dismiss(animated: false, completion: nil)
    }

    //MARK: Private functions
    
    private func getProfileViewController(segue: UIStoryboardSegue) -> ProfileViewController {
        guard let profileDetailViewController = segue.destination.childViewControllers[0] as? ProfileViewController else {
            fatalError("Unexpected destination: \(segue.destination)")
        }
        return profileDetailViewController
    }
    
    private func setTextViewText(field: UITextView, text: String) {
        let textWithFont = NSString(format:"<div style=\"font-family: '-apple-system', 'HelveticaNeue'; font-size: 14\">%@</div>" as NSString, text) as String
        do {
            let attrStr = try NSAttributedString(
                data: textWithFont.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                documentAttributes: nil
            )
            field.attributedText = attrStr
        } catch  {
            Log.error("Error setting label: %s", args: error.localizedDescription)
        }
    }
}
