import Foundation
import SwiftyJSON

class MaybeAddDateField : AssetOperationPhase {
    var name: String
    
    private var yesPhase: String
    private var noPhase: String
    private var parent: UIViewController
    
    init(_ name: String, parent: UIViewController, yesPhase: String, noPhase: String) {
        self.name = name
        self.parent = parent
        self.yesPhase = yesPhase
        self.noPhase = noPhase
    }
    
    func run(
        currentPhase: String,
        state: AssetOperationState,
        onSuccess: @escaping (AssetOperationPhase, AssetOperationState) -> Void,
        onError: @escaping (AssetOperationPhase, AssetOperationState) -> Void
    ) {
        if !Settings.get().autoAddDateField() {
            triggerNo(state: state, callback: onError)
            return
        }
        let typeName = state.asset.type?.dictionary?["name"]?.stringValue
        let alert = UIAlertController(
            title: "Question",
            message: "Field \"\(state.profile.fieldName)\" does not exist in asset #\(state.asset.id) of type \"\(typeName!)\". Do you want to add it?",
            preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "Yes", style: .default) { action in
            self.triggerYes(state: state, callback: onSuccess)
        })
        alert.addAction(UIAlertAction(title: "No", style: .cancel) { action in
            self.triggerNo(state: state, callback: onError)
        })
        parent.present(alert, animated: true)
    }
    
    func getOnSuccessPhaseName() -> String {
        return yesPhase
    }
    
    func getOnErrorPhaseName() -> String {
        return noPhase
    }
    
    private func triggerYes(state: AssetOperationState, callback: @escaping (AssetOperationPhase, AssetOperationState) -> Void) {
        callback(self, state)
    }
    
    private func triggerNo(
        state: AssetOperationState, callback: @escaping (AssetOperationPhase, AssetOperationState) -> Void
    ) {
        let typeName = state.asset.type?.dictionary?["name"]?.stringValue
        state.errorMessage = "Field \"\(state.profile.fieldName)\" does not exist in asset #\(state.asset.id) of type \"\(typeName!)\""
        callback(self, state)
    }
}
