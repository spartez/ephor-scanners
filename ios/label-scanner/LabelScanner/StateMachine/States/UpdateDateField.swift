import Foundation
import SwiftyJSON

class UpdateDateField : AssetOperationPhase {
    var name: String
    
    private var successPhase: String
    private var errorPhase: String
    
    private var network = Network()
    private var analytics = Analytics()
    
    init(_ name: String, successPhase: String, errorPhase: String) {
        self.name = name
        self.successPhase = successPhase
        self.errorPhase = errorPhase
    }
    
    func run(
        currentPhase: String,
        state: AssetOperationState,
        onSuccess: @escaping (AssetOperationPhase, AssetOperationState) -> Void,
        onError: @escaping (AssetOperationPhase, AssetOperationState) -> Void
    ) {
        guard let id = state.fieldDefinition?.dictionary?["id"]?.uInt as UInt! else {
            state.errorMessage = "Invalid field type definition"
            onError(self, state)
            return
        }
        
        let name = state.fieldDefinition?.dictionary?["name"]?.string
        
        network.post(
            state.profile.url + "/rest/com-spartez-ephor/1.0/item/\(state.asset.id)/field/-1?fielddef=\(id)",
            user: state.profile.user,
            password: state.profile.password,
            data: [ Date().getTime() ],
            onSuccess: { optJson in
                if optJson != nil {
                    self.analytics.trackDateFieldUpdate(field: name!)
                    onSuccess(self, state)
                } else {
                    state.errorMessage = "Unexpected empty response"
                    onError(self, state)
                }
            },
            onError: { error in
                state.errorMessage = error
                onError(self, state)
            }
        )
    }
    
    func getOnSuccessPhaseName() -> String {
        return successPhase
    }
    
    func getOnErrorPhaseName() -> String {
        return errorPhase
    }
}
