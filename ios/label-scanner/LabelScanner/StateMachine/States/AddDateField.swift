import Foundation
import UIKit

class AddDateField : AssetOperationPhase {
    var name: String
    
    private var successPhase: String
    private var errorPhase: String
    private var parent: UIViewController
    
    private var network = Network()
    
    init(_ name: String, parent: UIViewController, successPhase: String, errorPhase: String) {
        self.name = name
        self.parent = parent
        self.successPhase = successPhase
        self.errorPhase = errorPhase
    }
    
    func run(
        currentPhase: String,
        state: AssetOperationState,
        onSuccess: @escaping (AssetOperationPhase, AssetOperationState) -> Void,
        onError: @escaping (AssetOperationPhase, AssetOperationState) -> Void
    ) {
        guard let type = state.asset.type?.dictionary?["dottedName"]?.stringValue else {
            state.errorMessage = "Asset type object does not have \"name\" property"
            onError(self, state)
            return
        }
        let data = self.getFieldDefinition(state: state)
        network.post(
            state.profile.url + "/rest/com-spartez-ephor/1.0/itemtypefield/\(type)",
            user: state.profile.user,
            password: state.profile.password,
            data: data,
            onSuccess: { optJson in
                let alert = UIAlertController(
                    title: "Rebuild index",
                    message:
                        "You have added a field to asset type. Now you have to rebuild index so search functionality could work correctly. " +
                        "You can rebuild index in the Asset Tracker configuration in JIRA.",
                    preferredStyle: .alert
                )
                alert.addAction(UIAlertAction(title: "Close", style: .cancel) { action in
                    state.fieldDefinition = optJson
                    onSuccess(self, state)
                })
                self.parent.present(alert, animated: true)
            },
            onError: { error in
                state.errorMessage = error
                onError(self, state)
            }
        )
    }
    
    func getOnSuccessPhaseName() -> String {
        return successPhase
    }
    
    func getOnErrorPhaseName() -> String {
        return errorPhase
    }
    
    private func getFieldDefinition(state: AssetOperationState) -> [String:Any] {
        return [
            "type": state.fieldType?.dictionary?["id"]?.uInt!,
            "name": state.fieldType?.dictionary?["defaultTitle"]?.stringValue
        ]
    }
}
