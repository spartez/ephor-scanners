import Foundation
import SwiftyJSON

class GetDateField : AssetOperationPhase {
    var name: String
    
    private var successPhase: String
    private var errorPhase: String
    
    private var network = Network()
    
    init(_ name: String, successPhase: String, errorPhase: String) {
        self.name = name
        self.successPhase = successPhase
        self.errorPhase = errorPhase
    }
    
    func run(
        currentPhase: String,
        state: AssetOperationState,
        onSuccess: @escaping (AssetOperationPhase, AssetOperationState) -> Void,
        onError: @escaping (AssetOperationPhase, AssetOperationState) -> Void
        ) {
        network.get(
            state.profile.url + "/rest/com-spartez-ephor/1.0/fieldtype/\(state.profile.fieldName)",
            user: state.profile.user,
            password: state.profile.password,
            onSuccess: { optJson in
                guard let json = optJson as? JSON else {
                    state.errorMessage = "Unexpected response - no field type data"
                    onError(self, state)
                    return
                }
                state.fieldType = json
                onSuccess(self, state)
            },
            onError: { error in
                state.errorMessage = error
                onError(self, state)
            }
        )
    }
    
    func getOnSuccessPhaseName() -> String {
        return successPhase
    }
    
    func getOnErrorPhaseName() -> String {
        return errorPhase
    }
}

