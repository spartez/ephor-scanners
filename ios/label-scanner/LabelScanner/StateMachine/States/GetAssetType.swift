import Foundation
import SwiftyJSON

class GetAssetType : AssetOperationPhase {
    var name: String
    
    private var haveFieldPhase: String
    private var dontHaveFieldPhase: String
    private var errorPhase: String
    private var haveField = false
    
    private var network = Network()
    
    init(_ name: String, haveFieldPhase: String, dontHaveFieldPhase: String, errorPhase: String) {
        self.name = name
        self.haveFieldPhase = haveFieldPhase
        self.dontHaveFieldPhase = dontHaveFieldPhase
        self.errorPhase = errorPhase
    }
    
    func run(
        currentPhase: String,
        state: AssetOperationState,
        onSuccess: @escaping (AssetOperationPhase, AssetOperationState) -> Void,
        onError: @escaping (AssetOperationPhase, AssetOperationState) -> Void
    ) {
        haveField = false
        let type = state.asset.json?.dictionary?["type"]?.uInt;
        if type == nil {
            state.errorMessage = "Asset object does not have \"type\" property"
            onError(self, state)
            return;
        }
        network.get(
            state.profile.url + "/rest/com-spartez-ephor/1.0/itemtype/\(type!)",
            user: state.profile.user,
            password: state.profile.password,
            onSuccess: { optJson in
                guard let json = optJson else {
                    state.errorMessage = "Unexpected response - no asset type data"
                    onError(self, state)
                    return
                }
                self.handleResult(state: state, assetType: json)
                onSuccess(self, state)
        },
            onError: { error in
                state.errorMessage = error
                onError(self, state)
        }
        )
    }
    
    func getOnSuccessPhaseName() -> String {
        return haveField ? haveFieldPhase : dontHaveFieldPhase
    }
    
    func getOnErrorPhaseName() -> String {
        return errorPhase
    }
    
    func handleResult(state: AssetOperationState, assetType: JSON) {
        state.asset.type = assetType
        guard let fields = assetType.dictionary?["fields"] else {
            return
        }
        if fields.array?.count == 0 {
            return
        }
        for index in 0...(fields.array?.count)! - 1 {
            guard let field = fields.array?[index] else {
                continue
            }
            let fieldName = field.dictionary?["type"]?.dictionary?["name"]?.stringValue
            if fieldName == state.profile.fieldName {
                state.fieldDefinition = field
                self.haveField = true
                break
            }
        }
    }
}
