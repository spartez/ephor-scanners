import Foundation

class RunStoredOperation : AssetOperationPhase {
    var name: String

    private var successPhase: String
    private var errorPhase: String
    private var network = Network()
    private var analytics = Analytics()
    
    init(_ name: String, successPhase: String, errorPhase: String) {
        self.name = name
        self.successPhase = successPhase
        self.errorPhase = errorPhase
    }
    
    func run(
        currentPhase: String,
        state: AssetOperationState,
        onSuccess: @escaping (AssetOperationPhase, AssetOperationState) -> Void,
        onError: @escaping (AssetOperationPhase, AssetOperationState) -> Void
    ) {
        let data = [ Int(state.asset.id) ]
        network.post(
            state.profile.url + "/rest/com-spartez-ephor/1.0/stored-operation/\(state.profile.operationId!)/execute",
            user: state.profile.user,
            password: state.profile.password,
            data: data,
            onSuccess: { optJson in
                self.analytics.trackOperationSequence(profile: state.profile)
                onSuccess(self, state)
            },
            onError: { error in
                state.errorMessage = error
                onError(self, state)
            }
        )
    }
    
    func getOnSuccessPhaseName() -> String {
        return successPhase
    }
    
    func getOnErrorPhaseName() -> String {
        return errorPhase
    }
}
