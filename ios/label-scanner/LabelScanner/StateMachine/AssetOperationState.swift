import Foundation
import SwiftyJSON

class AssetOperationState {
    var asset: Asset
    var profile: Profile
    
    init(asset: Asset, profile: Profile) {
        self.asset = asset
        self.profile = profile
    }
    
    var fieldType: JSON?
    var fieldDefinition: JSON?
    var errorMessage: String?
}

protocol AssetOperationPhase {
    var name: String { get }
    
    func run(
        currentPhase: String,state: AssetOperationState,
        onSuccess: @escaping (_ phase: AssetOperationPhase, _ state: AssetOperationState) -> Void,
        onError: @escaping (_ phase: AssetOperationPhase, _ state: AssetOperationState) -> Void
    )
    
    func getOnSuccessPhaseName() -> String
    func getOnErrorPhaseName() -> String
}

