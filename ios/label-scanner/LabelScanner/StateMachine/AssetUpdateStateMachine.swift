import Foundation

class AssetOperationStateMachine : NSObject {
    var phases = [String:AssetOperationPhase]()
    var profile: Profile
    
    init?(profile: Profile, phases: [AssetOperationPhase]) {
        if phases.count == 0 {
            return nil
        }
        var haveStart = false
        for index in 0...phases.count - 1 {
            if phases[index].name == "start" {
                haveStart = true
            }
            self.phases[phases[index].name] = phases[index]
        }
        if !haveStart {
            return nil
        }
        self.profile = profile
    }

    func run(
        asset: Asset,
        onUpdateSuccess: @escaping (_ state: AssetOperationState) -> Void,
        onUpdateError: @escaping (_ state: AssetOperationState) -> Void
    ) {
        let state = AssetOperationState(asset: asset, profile: profile)
       
        phases["end"] = EndPhase(name: "end", callback: onUpdateSuccess)
        phases["error"] = EndPhase(name: "error", callback: onUpdateError)

        let start = phases["start"]!
        
        start.run(
            currentPhase: "start",
            state: state,
            onSuccess: { phase, state in
                self.onSuccess(phase: start, state: state, onUpdateSuccess: onUpdateSuccess, onUpdateError: onUpdateError)
            },
            onError: { phase, state in
                self.onError(phase: start, state: state, onUpdateSuccess: onUpdateSuccess, onUpdateError: onUpdateError)
            }
        )
    }
    
    private class EndPhase : AssetOperationPhase {
        var name: String
        var callback: (_ state: AssetOperationState) -> Void
        
        init(name: String, callback: @escaping (_ state: AssetOperationState) -> Void) {
            self.name = name
            self.callback = callback
        }
        
        func run(
            currentPhase: String,state: AssetOperationState,
            onSuccess: @escaping (_ phase: AssetOperationPhase, _ state: AssetOperationState) -> Void,
            onError: @escaping (_ phase: AssetOperationPhase, _ state: AssetOperationState) -> Void
        ) {
            return callback(state)
        }
        
        func getOnSuccessPhaseName() -> String { return "" }
        func getOnErrorPhaseName() -> String { return "" }
    }

    private func onSuccess(
        phase: AssetOperationPhase,
        state: AssetOperationState,
        onUpdateSuccess: @escaping (_ state: AssetOperationState) -> Void,
        onUpdateError: @escaping (_ state: AssetOperationState) -> Void
    ) {
        runPhase(phase.getOnSuccessPhaseName(), state: state, onUpdateSuccess: onUpdateSuccess, onUpdateError: onUpdateError)
    }
    
    private func onError(
        phase: AssetOperationPhase,
        state: AssetOperationState,
        onUpdateSuccess: @escaping (_ state: AssetOperationState) -> Void,
        onUpdateError: @escaping (_ state: AssetOperationState) -> Void
    ) {
        runPhase(phase.getOnErrorPhaseName(), state: state, onUpdateSuccess: onUpdateSuccess, onUpdateError: onUpdateError)
    }
    
    private func runPhase(
        _ name: String,
        state: AssetOperationState,
        onUpdateSuccess: @escaping (_ state: AssetOperationState) -> Void,
        onUpdateError: @escaping (_ state: AssetOperationState) -> Void
    ) {
        if let phase = phases[name] {
            phase.run(currentPhase: name, state: state,
                onSuccess: { phase, state in
                    self.onSuccess(phase: phase, state: state, onUpdateSuccess: onUpdateSuccess, onUpdateError: onUpdateError)
                },
                onError: { phase, state in
                    self.onError(phase: phase, state: state, onUpdateSuccess: onUpdateSuccess, onUpdateError: onUpdateError)
                }
            )
        } else {
            state.errorMessage = "State machine error, no worker state for \"" + name + "\""
            onUpdateError(state)
        }
    }
}

