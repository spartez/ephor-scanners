//
//  Analytics.swift
//  LabelScanner
//
//  Created by Victor Debone on 07/06/2018.
//  Copyright © 2018 kalamon. All rights reserved.
//

import Foundation
import KeenClient

class Analytics {
    private var network = Network()
    
    func startTracking() {
        KeenClient.disableGeoLocation()
        KeenClient.sharedClient(
            withProjectID: "5b0e9af1c9e77c0001398e46",
            andWriteKey: "058960F245F4D9FAD154E89AFF6C3B5AABB2311A37E9DC44DDFE47AB792A2E9A2CA452CA2AF253228DC8A9FDEB90515B767FBCE5634B3E1C04D05E6A6599A3D18300347E703D068FD4DE8FD042FE1C6A64201AA7D81DAD127844C0F8E0EA0553",
            andReadKey: "nothingtoseehere"
        )
    
        let iden: String = (UIDevice.current.identifierForVendor?.uuidString)!
        
        KeenClient.shared().globalPropertiesDictionary = [
            "appInstallationId": iden,
            "os": "iOS"
        ]
    }
    
    func addData(event: [String:Any], collection: String) {
        do {
            try KeenClient.shared().addEvent(event, toEventCollection: collection)
        } catch _ {
        };
    }
    
    func sendData(application: UIApplication) {
        let taskId : UIBackgroundTaskIdentifier = application.beginBackgroundTask(expirationHandler: {() -> Void in
            NSLog("Background task is being expired.")
        });
        KeenClient.shared().upload(finishedBlock: {() -> Void in
            application.endBackgroundTask(taskId)});
    }
    
    func trackAppStart(profilesNumber: Int) {
        self.addData(event: ["profileCount": profilesNumber], collection: "mobile app started")
    }
    
    func trackDateFieldUpdate(field: String) {
        self.addData(event: ["fieldName": field], collection: "mobile date field update")
    }
    
    func trackOperationSequence(profile: Profile) {
        network.get(
            profile.url + "/rest/com-spartez-ephor/1.0/stored-operation/\(profile.operationId!)",
            user: profile.user,
            password: profile.password,
            onSuccess: { optJson in
                let operations = optJson?.dictionaryObject?["operations"] as! NSArray
                
                let fields = operations.map{($0 as! NSDictionary)["fieldName"] as! String}
                
                self.addData(event: [
                    "id": profile.operationId!,
                    "name": profile.operationName,
                    "fields": fields.joined(separator: ",")
                ], collection: "mobile actions")
            },
            onError: { _ in }
        )
    }
}
