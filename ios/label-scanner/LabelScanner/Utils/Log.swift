import Foundation
import os.log

class Log {
    class func debug(_ text: String) {
        print("DEBUG: " + text)
//        os_log(text, log: OSLog.default, type: .debug)
    }
    
    class func debug(_ format: String, args: Any...) {
        print(String(format: "DEBUG: " + format, args))
    }
    
    class func error(_ text: String) {
        print("DEBUG: " + text)
        //        os_log(text, log: OSLog.default, type: .debug)
    }
    
    class func error(_ format: String, args: Any...) {
        print(String(format: "DEBUG: " + format, args))
    }
}
