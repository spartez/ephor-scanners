import Foundation
import Alamofire
import SwiftyJSON

public class Network {
    var sessionManager: SessionManager?
    
    init() {
        sessionManager = nil
    }

    func get(
        _ url: String,
        user: String,
        password: String,
        onSuccess: @escaping (_ result: JSON?) -> Void,
        onError: @escaping (_ result: String) -> Void
    ) {
        reset()
        call(url, user: user, password: password, method: .get, data: nil, onSuccess: onSuccess, onError: onError)
    }
        
    func post(
        _ url: String,
        user: String,
        password: String,
        data: Any?,
        onSuccess: @escaping (_ result: JSON?) -> Void,
        onError: @escaping (_ result: String) -> Void
    ) {
        reset()
        call(url, user: user, password: password, method: .post, data: data, onSuccess: onSuccess, onError: onError)
    }

    //MARK: Private methods
    
    private func call(
        _ url: String,
        user: String,
        password: String,
        method: HTTPMethod,
        data: Any?,
        onSuccess: @escaping (_ result: JSON?) -> Void,
        onError: @escaping (_ result: String) -> Void
    ) {
        var headers: HTTPHeaders = [:]
        
        if let authorizationHeader = Request.authorizationHeader(user: user, password: password) {
            headers[authorizationHeader.key] = authorizationHeader.value
        }
        
        do {
            var urlRequest = try URLRequest(url: url, method: method, headers: headers)
            urlRequest.cachePolicy = .reloadIgnoringCacheData
            if data != nil && method != .get {
                if let parameters = data as? Parameters {
                    urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
                } else {
                    urlRequest = try urlRequest.asURLRequest()
                    let json = try JSONSerialization.data(withJSONObject: data!, options: [])
                    
                    if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
                        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
                    }
                    
                    urlRequest.httpBody = json
                }
            } else {
                urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)
            }
            sessionManager!
                .request(urlRequest)
                .authenticate(user: user, password: password)
                .validate()
                .responseData { response in
                    switch response.result {
                    case .success:
                        guard let data = response.data else {
                            onSuccess(nil)
                            return
                        }
                        let responseJSON = JSON(data: data)
                        onSuccess(responseJSON.isEmpty ? JSON.null : responseJSON)
                    case .failure(let error):
                        var errorMessage = error.localizedDescription
                        
                        if let data = response.data {
                            let responseJSON = JSON(data: data)
                            
                            if var message: String = responseJSON["message"].stringValue {
                                if !message.isEmpty {
                                    errorMessage = message
                                } else {
                                    message = String(data: response.data!, encoding: .utf8) ?? ""
                                    if !message.isEmpty {
                                        errorMessage = message
                                    }
                                }
                            }
                        }
                        onError(errorMessage)
                    }
            }
        } catch {
            onError(error.localizedDescription)
        }
    }

    // this is stupid, but if we don't reinitialize session manager (even ephemeral)
    // credentials somehow seem to be stored persistently after successful call, so
    // calling again with wrong credentials still succeeds even though it should not
    private func reset() {
        let configuration = URLSessionConfiguration.ephemeral
        
        configuration.urlCache = nil
        configuration.urlCredentialStorage = nil
        var timeout = Double(Settings.get().connectionTimeout())
        configuration.timeoutIntervalForRequest = timeout
        configuration.timeoutIntervalForResource = timeout
        sessionManager = Alamofire.SessionManager(configuration: configuration)
    }
}
