import UIKit

class Throbber: UIView {
    var activityView: UIActivityIndicatorView?
    
    public init() {
        super.init(frame: CGRect.zero)
        backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }
    
    var visible: Bool { get {
            return activityView != nil
        }
    }
    
    func show(parent: UIView) {
        hide()
        
        activityView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        activityView!.center = center
        activityView!.startAnimating()
        addSubview(activityView!)
        parent.addSubview(self)
        frame = CGRect(x: parent.frame.origin.x, y: parent.frame.origin.y, width: parent.frame.width, height: parent.frame.height)
        let throbberSize = activityView!.frame.size
        activityView!.frame = CGRect(
            x: frame.width / 2 - throbberSize.width / 2,
            y: frame.height / 2 - throbberSize.height / 2,
            width: throbberSize.width,
            height: throbberSize.height
        )
        parent.bringSubview(toFront: self)
    }
    
    func hide() {
        if let throbber = activityView {
            throbber.stopAnimating()
            throbber.removeFromSuperview()
            removeFromSuperview()
            activityView = nil
        }
    }
}
