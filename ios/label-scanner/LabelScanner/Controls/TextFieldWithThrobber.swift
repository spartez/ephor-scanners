import UIKit

class TextFieldWithThrobber: PaddedTextField {
    
    var fieldLoadingIndicator = UIActivityIndicatorView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        fieldLoadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        rightView = fieldLoadingIndicator
        rightViewMode = UITextFieldViewMode.always
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        let rightBounds = CGRect(x: bounds.size.width - 20, y: 0, width: 20, height: bounds.size.height)
        return rightBounds
    }
    
    func showThrobber() {
        fieldLoadingIndicator.startAnimating()
    }
    
    func hideThrobber() {
        fieldLoadingIndicator.stopAnimating()
    }
}
