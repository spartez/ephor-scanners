import UIKit

class TouchableView: UIView {

    var onTouch: TouchObserver? = nil
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if onTouch != nil {
            onTouch?.touchStarted()
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if onTouch != nil {
            onTouch?.touchEnded()
        }
    }
}

protocol TouchObserver {
    func touchStarted()
    func touchEnded()
}
