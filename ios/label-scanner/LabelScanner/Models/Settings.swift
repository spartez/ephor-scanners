import Foundation

class Settings {
    
    private static var instance: Settings? = nil
    
    //MARK: Initialization
    
    class func get() -> Settings {
        if instance == nil {
            instance = Settings()
        }
        return instance!
    }
    
    init() {
    }

    func connectionTimeout() -> Int {
        return getInt(key: "connectionTimeout")
    }
    
    func shutterSoundOn() -> Bool {
        return getBool(key: "shutterSoundOn")
    }
    
    func scanAutoStart() -> Bool {
        return getBool(key: "scanAutoStart")
    }
    
    func forceJiraUrl() -> Bool {
        return getBool(key: "forceJiraUrl")
    }
    
    func autoAddDateField() -> Bool {
        return getBool(key: "autoAddDateField")
    }
    
    private func getBool(key: String) -> Bool {
        UserDefaults.standard.synchronize()
        
        var val = UserDefaults.standard.value(forKey: key)
        if val == nil {
            val = defaults[key] as! Bool
        }
        return val as! Bool
    }
    
    private func getInt(key: String) -> Int {
        UserDefaults.standard.synchronize()
        
        var val = UserDefaults.standard.value(forKey: key)
        if val == nil {
            val = defaults[key] as! Int
        }
        return val as! Int
    }
    
    private var defaults: [String:Any] = [
        "connectionTimeout": 5,
        "shutterSoundOn": true,
        "scanAutoStart": true,
        "forceJiraUrl": false,
        "autoAddDateField": true
    ]
}
