import Foundation
import SwiftyJSON

class Asset : NSObject {

    var id: UInt
    var url: String
    var json: JSON? = nil
    var type: JSON? = nil
    
    init?(url: String) {
        self.url = url
        let servletIndex = url.range(of: "/plugins/servlet/com.spartez.ephor/item")?.lowerBound
        if servletIndex == nil {
            return nil
        }
        let idIndex = url.range(of: "/", options: .backwards)?.lowerBound
        let idStr = idIndex.map(url.substring(from:))
        if idStr == nil {
            return nil
        }
        do {
            id = UInt(idStr!.substring(from: idStr!.index(idStr!.startIndex, offsetBy: 1))) ?? 0
            if id == 0 {
                return nil
            }
        } catch {
            return nil
        }
    }
}
