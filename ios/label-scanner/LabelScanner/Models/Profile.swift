import UIKit

class Profile: NSObject, NSCoding, NSCopying {
    
    //MARK: Properties
    
    var url: String
    var user: String
    var password: String
    var fieldName: String
    var operationId: UInt?
    var operationName: String
    var dateFieldType: Bool
 
    //MARK: Archiving Paths
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("profiles")
    
    //MARK: Types
    
    struct PropertyKey {
        static let url = "url"
        static let user = "user"
        static let password = "password"
        static let fieldName = "fieldName"
        static let operationId = "operationId"
        static let operationName = "operationName"
        static let dateFieldType = "dateFieldType"
    }
    
    //MARK: Initialization
    
    init?(url: String, user: String, password: String, dateFieldType: Bool, fieldName: String, operationId: UInt, operationName: String) {
        guard !url.isEmpty && !user.isEmpty else {
            return nil
        }
        
        if dateFieldType {
            guard !fieldName.isEmpty else {
                return nil
            }
        } else {
            guard operationId > 0 && !operationName.isEmpty else {
                return nil
            }
        }
        
        // Initialize stored properties.
        self.url = url
        self.user = user
        self.password = password
        self.dateFieldType = dateFieldType
        self.fieldName = fieldName
        self.operationId = operationId
        self.operationName = operationName
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        guard let url = aDecoder.decodeObject(forKey: PropertyKey.url) as? String else {
            Log.debug("Unable to decode the url for a Profile object.")
            return nil
        }
        
        guard let user = aDecoder.decodeObject(forKey: PropertyKey.user) as? String else {
            Log.debug("Unable to decode the user for a Profile object.")
            return nil
        }

        guard let password = aDecoder.decodeObject(forKey: PropertyKey.password) as? String else {
            Log.debug("Unable to decode the password for a Profile object.")
            return nil
        }

        guard let dateFieldType = aDecoder.decodeObject(forKey: PropertyKey.dateFieldType) as? String else {
            Log.debug("Unable to decode the dateFieldType for a Profile object.")
            return nil
        }

        guard let fieldName = aDecoder.decodeObject(forKey: PropertyKey.fieldName) as? String else {
            Log.debug("Unable to decode the fieldName for a Profile object.")
            return nil
        }

        guard let operationId = aDecoder.decodeObject(forKey: PropertyKey.operationId) as? UInt else {
            Log.debug("Unable to decode the operationId for a Profile object.")
            return nil
        }

        guard let operationName = aDecoder.decodeObject(forKey: PropertyKey.operationName) as? String else {
            Log.debug("Unable to decode the operationName for a Profile object.")
            return nil
        }

        self.init(url: url, user: user, password: password, dateFieldType: dateFieldType == "true", fieldName: fieldName, operationId: operationId, operationName: operationName)
    }
    
    //MARK: NSCoding
    func encode(with aCoder: NSCoder) {
        aCoder.encode(url, forKey: PropertyKey.url)
        aCoder.encode(user, forKey: PropertyKey.user)
        aCoder.encode(password, forKey: PropertyKey.password)
        aCoder.encode(fieldName, forKey: PropertyKey.fieldName)
        aCoder.encode(operationId, forKey: PropertyKey.operationId)
        aCoder.encode(operationName, forKey: PropertyKey.operationName)
        aCoder.encode(dateFieldType ? "true" : "false", forKey: PropertyKey.dateFieldType)
    }
    
    //MARK: NSCopying
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = Profile(url: url, user: user, password: password, dateFieldType: dateFieldType, fieldName: fieldName, operationId: operationId ?? 0, operationName: operationName)
        return copy as Any
    }
}
