﻿using System.Collections.Generic;
using System.Linq;
using System.Management;

namespace ephor.scanners.wmibrowser.wmi {
    internal class WmiQuery {
        public IEnumerable<ManagementObject> GetNamespaces() {
            var nsClass = new ManagementClass(
                new ManagementScope("root"), 
                new ManagementPath("__namespace"), 
                null
            );
            return nsClass.GetInstances().Cast<ManagementObject>().OrderBy(o => o["Name"]);
        }

        public IEnumerable<ManagementObject> GetObjectClassesFromNamespace(ManagementObject ns) {
            var path = new ManagementPath("root\\" + ns.GetPropertyValue("Name"));
            var newClass = new ManagementClass(path);
            var options = new EnumerationOptions { EnumerateDeep = true };
            return newClass.GetSubclasses(options).Cast<ManagementObject>().OrderBy(o => o["__CLASS"]);
        }

        public IEnumerable<ManagementBaseObject> GetObjectsFromClass(ManagementObject clazz) {
            var ms = new ManagementScope();
            var wql = new ObjectQuery("select * from " + clazz["__CLASS"]);
            var searcher = new ManagementObjectSearcher(ms, wql);
            var oc = searcher.Get();
            var oe = oc.GetEnumerator();
            while (oe.MoveNext()) {
                yield return oe.Current;
            }
        }
    }
}
