﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using ephor.scanners.wmibrowser.wmi;

namespace ephor.scanners.wmibrowser {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow {

        private int cimv2Idx = -1;
        public MainWindow() {
            InitializeComponent();
            if (cimv2Idx >= 0) {
                System.Timers.Timer timer = new System.Timers.Timer { Interval = 100 };
                timer.Elapsed += (s, e) => {
                    timer.Stop();
                    cbNamespaces.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() => {
                        cbNamespaces.SelectedIndex = cimv2Idx;
                    }));
                };
                timer.Start();
            }
        }

        private void cbNamespaces_Initialized(object sender, EventArgs e) {
            var wmiQuery = new WmiQuery();
            var objects = wmiQuery.GetNamespaces();
            cbNamespaces.Items.Add(new NamespaceObject(null));
            int idx = 1;
            foreach (var o in objects) {
                cbNamespaces.Items.Add(new NamespaceObject(o));
                if (o != null && o["Name"].Equals("CIMV2")) {
                    cimv2Idx = idx;
                }
                ++idx;
            }
            if (textProperties != null) {
                textProperties.Text = "";
            }
        }

        private void cbNamespaces_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (cbClasses != null) {
                cbClasses.Items.Clear();
            }
            if (cbObjects != null) {
                cbObjects.Items.Clear();
            }
            if (textProperties != null) {
                textProperties.Text = "";
            }
            var item = cbNamespaces.SelectedItem as NamespaceObject;
            if (item == null || item.Ns == null) {
                return;
            }

            SetAllEnabled(false);

            labelPleaseWait.Visibility = Visibility.Visible;
            ThreadPool.QueueUserWorkItem(o => {
                var wmiQuery = new WmiQuery();
                var classes = wmiQuery.GetObjectClassesFromNamespace(item.Ns);
                if (classes == null) {
                    return;
                }
                classes = classes.ToArray();
                cbClasses.Dispatcher.Invoke(
                    DispatcherPriority.Normal, new Action(() => {
                        cbClasses.Items.Add(new ClassObject(null));
                        foreach (var clazz in classes) {
                            cbClasses.Items.Add(new ClassObject(clazz));
                        }
                        cbClasses.SelectedIndex = 0;
                        labelPleaseWait.Visibility = Visibility.Hidden;
                        cbNamespaces.IsEnabled = true;
                        cbClasses.IsEnabled = cbClasses.Items.Count > 1;
                    })
                );
            });
        }

        private void cbObjects_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var wrapper = cbObjects.SelectedItem as ManagementObjectWrapper;
            if (wrapper == null) {
                return;
            }
            object selectedItem = wrapper.O;
            var obj = selectedItem as ManagementBaseObject;
            var props = obj.Properties;
            var sb = new StringBuilder();
            int propNameLength = (from PropertyData prop in props select prop.Name.Length).Concat(new[] {0}).Max();

            foreach (var prop in props) {
                if (prop.Value != null) {
                    sb.Append(FillLeft(prop.Name, propNameLength)).Append(": ").Append(prop.Value).Append("\n");
                } else {
                    sb.Append(FillLeft(prop.Name, propNameLength)).Append(": ").Append("<null>").Append("\n");
                }
            }
            textProperties.Text = sb.ToString();
        }

        private string FillLeft(string name, int toLength) {
            return new String(name.ToCharArray()).PadLeft(toLength, ' ');
        }

        private void cbClasses_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (cbObjects != null) {
                cbObjects.Items.Clear();
            }
            var item = cbClasses.SelectedItem as ClassObject;
            if (item == null || item.Clazz == null) {
                return;
            }
            if (textProperties != null) {
                textProperties.Text = "";
            }

            labelPleaseWait.Visibility = Visibility.Visible;
            SetAllEnabled(false);
            ThreadPool.QueueUserWorkItem(o => {
                try {
                    var wmiQuery = new WmiQuery();
                    IEnumerable<ManagementBaseObject> objects = wmiQuery.GetObjectsFromClass(item.Clazz);
                    IList<ManagementObjectWrapper> objectsList = new List<ManagementObjectWrapper>();
                    foreach (var obj in objects) {
                        var wrapper = new ManagementObjectWrapper(obj);
                        objectsList.Add(wrapper);
                        showQueriedObject(wrapper);
                    }
                    objectsList = objectsList.OrderBy(obj => obj.ToString()).ToArray();
                    cbObjects.Dispatcher.Invoke(
                        DispatcherPriority.Normal, new Action(() => {
                            try {
                                foreach (var obj in objectsList) {
                                    cbObjects.Items.Add(obj);
                                }
                                labelPleaseWait.Visibility = Visibility.Hidden;
                                labelQueriedObject.Visibility = Visibility.Hidden;
                                cbNamespaces.IsEnabled = true;
                                cbClasses.IsEnabled = true;
                                if (cbObjects.Items.Count > 0) {
                                    cbObjects.IsEnabled = true;
                                    cbObjects.SelectedIndex = 0;
                                } else {
                                    cbObjects.IsEnabled = false;   
                                }
                            } catch (Exception ex) {
                                MessageBox.Show("Some error happened 1: " + ex.Message);
                                labelPleaseWait.Visibility = Visibility.Hidden;
                                cbNamespaces.IsEnabled = true;
                                cbClasses.IsEnabled = true;
                                cbObjects.IsEnabled = false;
                            }
                        })
                    );
                }
                catch (Exception ex) {
                    Debug.WriteLine(ex.ToString());
                    cbObjects.Dispatcher.Invoke(
                        DispatcherPriority.Normal, new Action(() => MessageBox.Show("Some error happened: " + ex.Message))
                    );
                }
            });
        }

        private void SetAllEnabled(bool enabled) {
            cbNamespaces.IsEnabled = enabled;
            cbClasses.IsEnabled = enabled;
            cbObjects.IsEnabled = enabled;
        }

        private void showQueriedObject(ManagementObjectWrapper obj) {
            labelQueriedObject.Dispatcher.Invoke(
                DispatcherPriority.Normal, new Action(() => {
                    labelQueriedObject.Visibility = Visibility.Visible;
                    labelPleaseWait.Visibility = Visibility.Hidden;
                    labelQueriedObject.Content = "Fetching " + obj.ToString() + "...";
                })
            );
        }

        private void Window_Initialized(object sender, EventArgs e) {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.FileVersion;
            labelVersion.Content = fvi.LegalCopyright + " " + fvi.CompanyName + ". Version " + version;
        }
    }

    internal class ClassObject {
        public ClassObject(ManagementObject clazz) {
            Clazz = clazz;
        }

        public ManagementObject Clazz { get; set; }

        public override string ToString() {
            return Clazz == null ? "<None>" : Clazz["__CLASS"].ToString();
        }
    }

    internal class NamespaceObject {
        public NamespaceObject(ManagementObject ns) {
            Ns = ns;
        }

        public ManagementObject Ns { get; set; }

        public override string ToString() {
            return Ns == null ? "<None>" : Ns["Name"].ToString();
        }
    }

    internal class ManagementObjectWrapper {
        private string name;

        public ManagementBaseObject O { get; set; }
        public ManagementObjectWrapper(ManagementBaseObject o) {
            O = o;
        }

        public override string ToString() {
            return GetName();
        }

        private string GetName() {
            if (name != null) {
                return name;
            }
            try {
                if (O == null) {
                    name = "<None>";
                } else {
                    name = O["Name"].ToString();
                }
            } catch (Exception) {
                name = O["__CLASS"].ToString();
//                foreach (var p in O.Properties) {
//                    Debug.WriteLine(p.Name + ": " + (p.Value ?? "<null>"));
//                }
//                foreach (var p in O.SystemProperties) {
//                    Debug.WriteLine(p.Name + ": " + (p.Value ?? "<null>"));
//                }
            }
            return name;
        }
    }
}
