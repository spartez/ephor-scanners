using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Text;

namespace ephor.scanners.commandline
{
    public class ConnectionTest
    {
        private readonly string jiraUrl;
        private readonly string username;
        private readonly string password;

        public ConnectionTest(string url, string username, string password)
        {
            jiraUrl = url;
            this.username = username;
            this.password = password;
        }

        public  void TestConnectionToJira( )
        {
            Console.WriteLine("Testing Jira connection, method GET on url:"+jiraUrl);
            Uri jiraUri = new Uri(jiraUrl);
            ServicePointManager.ServerCertificateValidationCallback += loggingCallback();
            WebRequest testRequest = WebRequest.Create(jiraUri);
            testRequest.Method = WebRequestMethods.Http.Get;        
            if (username != null && password != null ) {
               SetBasicAuthHeader(testRequest, username, password);
            }
            try
            {
                HttpWebResponse response = (HttpWebResponse) testRequest.GetResponse();   
                DebugHeaders(response);
                Console.WriteLine("Web response : "+response);
                Console.WriteLine("---------------------------- Web response body ----------------------");
                Console.WriteLine(""+new StreamReader(response.GetResponseStream()).ReadToEnd());
            }
            catch (WebException e)
            {
                Console.WriteLine("Web exception for requested uri");
                Console.WriteLine("WebException.status: "+e.Status);
                Console.WriteLine("WebException.message: "+e.Message);
                Console.WriteLine("WebException.source: "+e.Source);
                Console.WriteLine("WebException.stacktrace: "+e.StackTrace);
                Console.WriteLine("WebException.targetSite: "+e.TargetSite);
                if (e.Response != null)
                {
                    DebugExceptionResponseHeaders(e);
                }
                Console.WriteLine("WebException.end  --------------------");
            }
        }

        private static void DebugExceptionResponseHeaders(WebException e)
        {
            Console.WriteLine("WebException.Response.headres: ");
            foreach (string responseHeader in e.Response.Headers)
            {
                Console.WriteLine(responseHeader);
            }
        }

        private static void DebugHeaders(HttpWebResponse response)
        {
            Console.WriteLine("Response.headres: ");
            foreach (string responseHeader in response.Headers)
            {
                Console.WriteLine(responseHeader);
            }
        }

        private static RemoteCertificateValidationCallback loggingCallback()
        {
            return (sender, certificate, chain, errors) =>
            {
                Console.WriteLine("https debug:");
                Console.WriteLine("sender:"+sender);
                Console.WriteLine("certificate:"+certificate);
                Console.WriteLine("certificate.chain:"+chain);
                Console.WriteLine("certificate.errors:"+errors);               
                return true;
            };
        }

        private  void SetBasicAuthHeader(WebRequest req, string u, string p) {
            if (u == null || p == null) {
                return;
            }
            var authInfo = u + ":" + p;
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            req.Headers["Authorization"] = "Basic " + authInfo;
        }
    }
}