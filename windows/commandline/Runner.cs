﻿using System;
using System.Collections.Generic;
using System.Linq;
using ephor.scanners.api;
using ephor.scanners.net;

namespace ephor.scanners.commandline {
    internal class Runner {
        private readonly Uri ephorUri;
        private readonly string jiraUserName;
        private readonly string jiraPassword;
        private string jiraSecurityToken;
        private readonly string itemType;
        private readonly string categoryPath;
        private bool verbose;

        public Runner(
            Uri ephorUri, string jiraUserName, string jiraPassword, string jiraSecurityToken,
            string itemType, string categoryPath, bool verbose) {

            this.ephorUri = ephorUri;
            this.jiraUserName = jiraUserName;
            this.jiraPassword = jiraPassword;
            this.jiraSecurityToken = jiraSecurityToken;
            this.itemType = itemType;
            this.categoryPath = categoryPath;

            this.verbose = verbose;
        }

        public void Run(Scanners scanners) {
            var successfulScans = new List<string>();
            var createdItems = new List<string>();
            var updatedItems = new List<string>();
            foreach (var scanner in scanners.GetAvailableScanners()) {
                Console.WriteLine("Running " + scanner.Name + "...");
                scanner.Scan(
                    (ip, matchingQuery, scanResult, updateOnly) => {
                        successfulScans.Add(ip);
                        DumpScanResults(ip, scanResult);
                        Console.WriteLine("Posting host " + ip + " scan results to " + ephorUri);
                        var oldToken = jiraSecurityToken;
                        try
                        {
                            bool updated;
                            int itemId =
                                new EphorItemUpdater(
                                    ephorUri, jiraUserName, jiraPassword, jiraSecurityToken,
                                    categoryPath.Split(new[] { "/" }, StringSplitOptions.None), itemType, matchingQuery, updateOnly, scanResult, verbose, text => Console.WriteLine(text))
                                        .Update(out updated, out jiraSecurityToken);
                            if (updated) {
                                Console.WriteLine("Updated asset #" + itemId + " (" + ephorUri + "/plugins/servlet/com.spartez.ephor/item/" + itemId + ")");
                                updatedItems.Add(ip);
                            } else {
                                Console.WriteLine("Created asset #" + itemId + " (" + ephorUri + "/plugins/servlet/com.spartez.ephor/item/" + itemId + ")");
                                createdItems.Add(ip);
                            }
                        } catch (EphorItemUpdater.UpdaterException e) {
                            Console.WriteLine("Error posting data to Asset Tracker: " + e.Message);
                            jiraSecurityToken = e.token;
                        }
                        if (jiraUserName == null && (Equals(oldToken, jiraSecurityToken) || jiraSecurityToken == null)) {
                            Console.WriteLine("New security token not received from Asset Tracker, unable to continue updating");
                            return false;
                        }
                        return true;
                    },
                    (ip, e) => Console.WriteLine("Error scanning host " + ip + ": " + e.Message + dumpException(e)),
                    Console.WriteLine
                );
            }
            Console.WriteLine("Scanning finished");
            Console.WriteLine("Successfully scanned " + successfulScans.Count + " hosts");
            Console.WriteLine("Created " + createdItems.Count + " assets");
            Console.WriteLine("Updated " + updatedItems.Count + " assets");
        }

        private string dumpException(Exception e) {
            if (verbose) {
                if (e == null) {
                    return "none";
                }
                return "\nmessage: " + e.Message + "\nstack trace: " + e.StackTrace + "\ninner: " + dumpException(e.InnerException);
            }
            return "";
        }

        private void DumpScanResults(string ip, IDictionary<string, string> scanResult) {
            if (!verbose) {
                return;
            }
            Console.WriteLine("Scan results of host " + ip);
            foreach (var key in scanResult.Keys) {
                Console.WriteLine(key + "\n--------------\n" + scanResult[key] + "\n");
            }
            Console.WriteLine("\n\n");
        }
    }
}