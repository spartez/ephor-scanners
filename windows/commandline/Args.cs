﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ephor.scanners.commandline {
    internal class Args {
        private class Arg {
            public Arg(bool required, string description) : this(required, true, description) {}

            public Arg(bool required, bool needsValue, string description) {
                Required = required;
                NeedsValue = needsValue;
                Description = description;
            }

            public bool Required { get; private set; }
            public bool NeedsValue { get; set; }
            public string Description { get; set; }
            public string Value { get; set; }
        }

        public string this[string key]
        {
            get { return args.ContainsKey(key) ? args[key].Value : null; }
        }

        private readonly Dictionary<string, Arg> args = new Dictionary<string, Arg> {
            {"usage", new Arg(false, false, "Print usage")},
            {"jira-user", new Arg(false, "JIRA user name. Required if token is not supplied")},
            {"jira-password", new Arg(false, "JIRA user password. Required if token is not supplied")},
            {"jira-token", new Arg(false, true, "Security token. required if user/password is not supplied")},
            {"folder", new Arg(true, "Folder path (\"/\" as delimiter) for new assets")},
            {"type", new Arg(true, "Asset type for new assets")},
            {"wmi-config", new Arg(false, "WMI configuration file")},
            {"ip-range", new Arg(false, true, "Range of IP addresses to scan. For example: 192.168.1.1-192.168.1.100,192.168.2.1. Either this or --self must be provided")},
            {"self", new Arg(false, false, "Scan this computer only. No need for --wmi-user and --wmi-password. Either this or --ip-range must be provided") },
            {"wmi-user", new Arg(false, true, "WMI user account name. Use \"domain\\user\" if you need to supply domain name")},
            {"wmi-password", new Arg(false, true, "WMI user's password")},
            {"wmi-timeout", new Arg(false, true, "WMI scan timeout in seconds. Default is 20 seconds")},
            {"ping-first", new Arg(false, false, "Ping the scanned machine before attempting to scan it, to speed up scan of dead IP addresses")},
            {"ping-timeout", new Arg(false, true, "Ping timeout in seconds. Default is 20 seconds")},
            {"no-close", new Arg(false, false, "Don't automatically close the application console window")},
            {"verbose", new Arg(false, false, "Verbose output of scanning and updating process")},
            {"version", new Arg(false, false, "Print program version")},
            {"test-jira", new Arg(false, false, "test jira connection, use with user/pass(optional) and Jira url")},
        };

        public Args(IEnumerable<string> args) {
            string last = null;
            foreach (var arg in args) {
                last = arg;
                if (!arg.StartsWith("--")) {
                    continue;
                }
                var strings = new List<string>(arg.Split(new[] {'='}));
                var argName = strings[0].Substring(2);
                Arg a;
                if (!this.args.TryGetValue(argName, out a)) {
                    continue;
                }
                if (a.NeedsValue && strings.Count < 2) {
                    continue;
                }
                a.Value = !a.NeedsValue ? "set" : string.Join("=", strings.Skip(1));
            }
            if (last != null && !last.StartsWith("--")) {
                Url = last;
            }
        }

        public class ValidationError : Exception {
            public ValidationError(string message) : base(message) {}
        }

        public bool HasUsage
        {
            get { return args["usage"].Value != null; }
        }

        public string Url { get; private set; }

        public void Validate() {
            List<string> missing = (
                from arg
                in args.Where(arg => arg.Value.Required)
                where arg.Value.Value == null
                select arg.Key
            ).ToList();
            if (missing.Count <= 0) return;
            var message = string.Join(", ", missing.Select(arg => "--" + arg));
            if (Url == null) {
                const string urlRequired = "JIRA URL is required";
                message = string.IsNullOrEmpty(message) ? urlRequired : message + ", " + urlRequired;
            }
            if (missing.Count == 1) {
                throw new ValidationError("Argument: " + message + " is required");
            } else {
                throw new ValidationError("Arguments: " + message + " are required");
            }
        }

        public string Usage
        {
            get
            {
                var sb = new StringBuilder(
                    "Usage: " + AppDomain.CurrentDomain.FriendlyName + " [options] <jira-url>\n" +
                    "\n" +
                    "    Options:\n");
                foreach (var arg in args) {
                    sb.Append("        ")
                        .Append("--")
                        .Append(arg.Key)
                        .Append(arg.Value.Required ? "* - " : " - ")
                        .Append(arg.Value.Description)
                        .Append("\n");
                }
                sb.Append("    (*) arguments are required\n");
                return sb.ToString();
            }
        }

        public void ErrorUsage(Action<string> error) {
            error(Usage);
        }
    }
}