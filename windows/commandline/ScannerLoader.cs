﻿using ephor.scanners.api;
using ephor.scanners.wmi;

namespace ephor.scanners.commandline {
    internal class ScannerLoader {
        internal static Scanners LoadScanners(Args args) {
            var scanners = new Scanners();
            string ipRange = args["self"] == null ? args["ip-range"] : "127.0.0.1";

            scanners.RegisterScanner(
                new WmiScanner(
                    args["wmi-config"], 
                    args["wmi-user"], 
                    args["wmi-password"], 
                    ipRange,
                    args["wmi-timeout"],
                    args["ping-first"] != null,
                    args["ping-timeout"],
                    args["verbose"] != null
                ));
            return scanners;
        }
    }
}