﻿using System;
using System.Diagnostics;

namespace ephor.scanners.commandline {
    internal class Program {

        private static bool autoClose = true; 

        private static void Main(string[] args) {
            var arguments = new Args(args);
            autoClose = arguments["no-close"] == null;

            if (arguments["test-jira"] != null)
            {
                new ConnectionTest(arguments.Url, arguments["jira-user"], arguments["jira-password"]).TestConnectionToJira();
            }
            else
            {
                if (arguments.HasUsage)
                {
                    Console.WriteLine(arguments.Usage);
                    MaybeWaitForKey();
                    Environment.Exit(0);
                }

                if (arguments["version"] != null)
                {
                    Console.WriteLine("Version " + FileVersionInfo
                                          .GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location)
                                          .FileVersion);
                    MaybeWaitForKey();
                    Environment.Exit(0);
                }

                try
                {
                    arguments.Validate();

                    Uri uri = new Uri(arguments.Url);
                    string userName = arguments["jira-user"];
                    string password = arguments["jira-password"];
                    string token = arguments["jira-token"];
                    string itemType = arguments["type"];
                    string category = arguments["folder"];

                    var scanners = ScannerLoader.LoadScanners(arguments);

                    new Runner(uri, userName, password, token, itemType, category, arguments["verbose"] != null).Run(
                        scanners);

                    MaybeWaitForKey();
                }
                catch (Args.ValidationError e)
                {
                    arguments.ErrorUsage(txt => Error(e.Message + "\n" + txt));
                }
                catch (Exception e)
                {
#if DEBUG
                    Debug.WriteLine(e.ToString());
#endif
                    Error(e.Message);
                }
            }
        }

        private static void Error(string error) {
            Console.WriteLine(error);
            MaybeWaitForKey();
            Environment.Exit(1);
        }

        private static void MaybeWaitForKey() {
            if (autoClose) {
                return;
            }
            Console.WriteLine("Press any key to close the application");
            Console.ReadKey(true);
        }
    }
}