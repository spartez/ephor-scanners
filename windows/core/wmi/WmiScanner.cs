﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Management;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Xml.Linq;
using ephor.scanners.api;

namespace ephor.scanners.wmi {

    public class WmiScanner : Scanner {
        private readonly string wmiUser;
        private readonly string wmiPassword;
        private readonly string ipRange;
        private readonly int wmiTimeout;
        private readonly bool pingFirst;
        private readonly int pingTimeout;
        private readonly bool verbose;

        private class Field {
            public Field(string name, string objectClass) {
                this.name = name;
                this.objectClass = objectClass;
            }

            public readonly string name;
            public readonly string objectClass;
        }

        private class SubField {
            public SubField(string label, string propertyName, string format, uint mod) {
                this.label = label;
                this.propertyName = propertyName;
                this.format = format;
                this.mod = mod;
            }

            public readonly string label;
            public readonly string propertyName;
            public readonly string format;
            public readonly uint mod;
        }

        private class SingleField : Field {
            public SingleField(string name, string objectClass, string propertyName, string format, uint mod, string valueSeparator) : base(name, objectClass) {
                this.propertyName = propertyName;
                this.format = format;
                this.mod = mod;
                this.valueSeparator = valueSeparator;
            }

            public readonly string propertyName;
            public readonly string format;
            public readonly uint mod;
            public readonly string valueSeparator;
        }

        private class ComputeField : Field {
            public enum Op {
                add
            }

            public ComputeField(string name, string objectClass, string propertyName, string format, uint mod, string op) : base(name, objectClass) {
                this.single = new SingleField(name, objectClass, propertyName, format, mod, null);
                this.op = (Op) Enum.Parse(typeof(Op), op);
            }

            public UInt64 compute(IEnumerable<object> args) {
                if (op != Op.add) {
                    throw new ArgumentException("Only addition is supported");
                }
                UInt64 result = 0;
                foreach (var arg in args) {
                    result += UInt64.Parse(arg.ToString());
                }
                return result;
            }

            public readonly Op op;
            public readonly SingleField single;
        }

        private class MultiField : Field {
            public readonly List<SubField> subFields = new List<SubField>();
            public readonly string separator;
            public readonly string valueSeparator;
            public readonly string[] nullFieldsToSkip;
            public readonly string[] realMacFields;
            public readonly string filter;
            public readonly int merge;

            public MultiField(
                string name, string separator, string objectClass, string valueSeparator, 
                string[] nullFieldsToSkip, 
                string[] realMacFields,
                string filter, int merge) 
                    : base(name, objectClass) {
                this.separator = separator;
                this.valueSeparator = valueSeparator;
                this.nullFieldsToSkip = nullFieldsToSkip;
                this.realMacFields = realMacFields;
                this.filter = filter;
                this.merge = merge;
            }
        }

        private class Config {
            internal readonly IList<Field> fields = new List<Field> ();
        }

        private Config config = new Config();

        private string matchingField = null;
        private string matcherFilter = null;
        private bool updateOnly = false;

        public WmiScanner(
            string wmiConfigFile, string wmiUser, string wmiPassword, string ipRange, string wmiTimeoutString, bool pingFirst, string pingTimeoutString, bool verbose) {

            this.wmiUser = wmiUser;
            this.wmiPassword = wmiPassword;
            this.ipRange = ipRange;
            this.pingFirst = pingFirst;
            this.verbose = verbose;
            this.pingTimeout = pingTimeoutString != null ? int.Parse(pingTimeoutString) : 20;
            this.wmiTimeout = wmiTimeoutString != null ? int.Parse(wmiTimeoutString) : 20;
            LoadConfigFile(wmiConfigFile);
        }

        public string Name {
            get { return "WMI Scanner"; }
        }

        public void Scan(
            Func<string, string, Dictionary<string, string>, bool, bool> scanCallback, 
            Action<string, Exception> errorCallback, 
            Action<string> progressCallback
        ) {
            var localIps = getLocalIpAddresses();

            foreach (var address in getAddressesFromIpRange()) {
                try {
                    var results = new Dictionary<string, string>();

                    if (pingFirst && !pingHost(address, progressCallback)) {
                        continue;
                    }
                    var scope = createWmiScope(localIps, address, progressCallback);

                    foreach (var field in config.fields) {
                        HandleSingleField(scope, field as SingleField, results);
                        HandleComputeField(scope, field as ComputeField, results);
                        HandleMultiField(scope, field as MultiField, results);
                    }

                    if (!scanCallback.Invoke(address, matcherQuery(scope), results, updateOnly)) {
                        break;
                    }
                } catch (Exception e) {
                    errorCallback.Invoke(address, e);
                }
            }
        }

        private bool pingHost(string address, Action<string> progressCallback) {
            progressCallback.Invoke("Pinging host " + address + "...");
            var pingSender = new Ping();
            var options = new PingOptions { DontFragment = true };
            const string data = "WMI scan ping";
            byte[] buffer = Encoding.ASCII.GetBytes(data);
            var reply = pingSender.Send(IPAddress.Parse(address), pingTimeout * 1000, buffer, options);
            if (reply.Status == IPStatus.Success) {
                progressCallback.Invoke("success");
                return true;
            }
            progressCallback.Invoke("Ping result: " + reply.Status);
            return false;
        }

        private ManagementScope createWmiScope(IEnumerable<string> localIps, string address, Action<string> progressCallback) {
            if (IPAddress.IsLoopback(IPAddress.Parse(address)) || localIps.Contains(address)) {
                progressCallback.Invoke("Using local WMI scope for address " + address + " (this host address)");
                return new ManagementScope();
            }
            var userAndDomain = wmiUser.Split('\\');
            var user = userAndDomain.Length > 1 ? userAndDomain[1] : userAndDomain[0];
            var domain = userAndDomain.Length > 1 ? userAndDomain[0] : null;

            var options = new ConnectionOptions {
                Username = user,
                Password = wmiPassword,
                Authority = domain != null ? "NTLMDOMAIN:" + domain : null,
                Timeout = TimeSpan.FromSeconds(wmiTimeout)
            };

            // http://stackoverflow.com/questions/38859777/windows-10-wmic-wmi-remote-access-denied-with-local-administrator
            // https://msdn.microsoft.com/en-us/library/aa394603(v=vs.85).aspx
            var scope = new ManagementScope("\\\\" + address + "\\root\\cimv2", options);
            progressCallback.Invoke("Connecting to " + address + "...");
            scope.Connect();
            progressCallback.Invoke("Connected successfully, scanning...");
            return scope;
        }

        private List<string> getLocalIpAddresses() {
            List<string> result = new List<string>();
            var searcher = new ManagementObjectSearcher(new ManagementScope(), new ObjectQuery("SELECT * FROM Win32_NetworkAdapterConfiguration"));
            foreach (var wmi in searcher.Get()) {
                var val = wmi.GetPropertyValue("IPAddress");
                if (val != null) {
                    if (val.GetType().IsArray) {
                        result.AddRange(from object v in ((IEnumerable) val) select v.ToString());
                    } else {
                        result.Add(val.ToString());
                    }
                }
            }
            return result;
        }

        private string matcherQuery(ManagementScope scope) {
            string result = null;
            var query = "SELECT * FROM Win32_NetworkAdapter";
            if (matcherFilter != null) {
                query += " where " + matcherFilter;
            }
            try {
                var searcher = new ManagementObjectSearcher(scope, new ObjectQuery(query));
                var strings = new List<string>();
                foreach (var wmi in searcher.Get()) {
                    object description = wmi.GetPropertyValue("Description");
                    if (description != null && (
                            description.ToString().Contains("VirtualBox") ||
                            description.ToString().Contains("WFP Native MAC Layer LightWeight Filter-")
                        )
                    ) {
                        continue;
                    }
                    object val = wmi.GetPropertyValue("MACAddress");
                    if (val != null && macIsReal(val)) {
                        strings.Add(matchingField + "=\"" + val + "\"");
                    }
                }
                result = string.Join(" or ", strings);
            } catch (Exception) {
                if (verbose) {
                    Console.WriteLine("Error running query for MAC address: " + query);
                }
                throw;
            }
            if (String.IsNullOrEmpty(result)) {
                throw new Exception("Unable to create matcher query, empty MAC address list");
            }
            return result;
        }

        private void HandleSingleField(ManagementScope scope, SingleField field, Dictionary<string, string> results) {
            if (field == null) {
                return;
            }
            var value = GetStringFromObject(scope, field.objectClass, field.propertyName, field.mod, field.valueSeparator);
            if (field.name != null) {
                if (value != null && !string.IsNullOrEmpty(field.format)) {
                    results[field.name] = string.Format(field.format, value);
                } else {
                    results.SetValueIfNotNull(field.name, value);
                }
            }
        }

        private void HandleComputeField(ManagementScope scope, ComputeField field, Dictionary<string, string> results) {
            if (field == null) {
                return;
            }
            var searcher = new ManagementObjectSearcher(scope, new ObjectQuery("SELECT * FROM " + field.objectClass));
            List<object> vals = new List<object>();
            foreach (var wmi in searcher.Get()) {
                object val = wmi.GetPropertyValue(field.single.propertyName);
                if (val != null) {
                    vals.Add(val);
                }
            }
            var computed = field.compute(vals).Mod(field.single.mod);
            results[field.name] = string.Format(field.single.format, computed);
        }

        private void HandleMultiField(ManagementScope scope, MultiField field, Dictionary<string, string> results) {
            if (field == null) {
                return;
            }
            var sb = new StringBuilder();
            string query = "SELECT * FROM " + field.objectClass;
            if (field.filter != null) {
                query = query + " where " + field.filter;
            }
            try {
                var searcher = new ManagementObjectSearcher(scope, new ObjectQuery(query));
                foreach (var wmi in searcher.Get()) {
                    bool appended = false;
                    var sbb = new StringBuilder();
                    foreach (var sub in field.subFields) {
                        object val = wmi.GetPropertyValue(sub.propertyName).Mod(sub.mod);
                        if (val != null) {
                            if (field.realMacFields != null && field.realMacFields.Contains(sub.propertyName) && !macIsReal(val)) {
                                appended = false;
                                break;
                            }
                            sbb.Append(string.Format(sub.format, sub.label, val.ValString(field.valueSeparator))).Append("\n");
                            appended = true;
                        } else {
                            if (field.nullFieldsToSkip != null && !field.nullFieldsToSkip.Contains(sub.propertyName)) {
                                continue;
                            }
                            appended = false;
                            break;
                        }
                    }
                    if (appended) {
                        sb.Append(sbb).Append(field.separator);
                    }
                }
                if (field.name != null && sb.Length > 0) {
                    if (field.merge >= 0) {
                        results["--merge--." + field.merge + ".==." + field.name] = sb.ToString();
                    } else {
                        results[field.name] = sb.ToString();
                    }
                }
            } catch (Exception e) {
                if (verbose) {
                    Console.WriteLine("exception when handling multi field " + field.name + ", query: " + query + "\nexception: " + e.ToString());
                } else {
                    Console.WriteLine("exception when handling multi field " + field.name);
                }
                throw e;
            }
        }

        private bool macIsReal(object mac) {
            if (mac == null) {
                return false;
            }
            if (mac.ToString().Length < 12) {
                return false;
            }
            char c = mac.ToString().ToLowerInvariant()[1];
            if (c == '2' || c == '6' || c == 'a' || c == 'e') {
                return false;
            }
            return true;
        }

        private string GetStringFromObject(ManagementScope scope, string objectClass, string propertyName, uint mod = 0, string valueSeparator = null) {
            var searcher = new ManagementObjectSearcher(scope, new ObjectQuery("SELECT * FROM " + objectClass));
            foreach (var wmi in searcher.Get()) {
                object val = wmi.GetPropertyValue(propertyName);
                if (val == null) {
                    return null;
                }
                return val.Mod(mod).ValString(valueSeparator);
            }
            return null;
        }

        private static void MaybeDumpProperties(ManagementBaseObject wmi) {
            //#if DEBUG
            //            foreach (var property in wmi.Properties) {
            //                Debug.WriteLine(property.Name + "\t\t" + property.Value);
            //            }
            //            Debug.WriteLine("\n");
            //#endif
        }

        private IEnumerable<string> getAddressesFromIpRange() {
            List<string> result = new List<string>();
            string[] subRanges = ipRange.Split(',');
            foreach (var sub in subRanges) {
                var subRange = sub.Trim();
                var ends = subRange.Split('-');
                if (ends.Length == 1) {
                    result.Add(ends[0]);
                } else if (ends.Length == 2) {
                    IPAddress startIp = IPAddress.Parse(ends[0].Trim());
                    IPAddress endIp = IPAddress.Parse(ends[1].Trim());
                    uint startInt = startIp.toInt();
                    uint endInt = endIp.toInt();
                    for (var ip = startInt; ip <= endInt; ++ip) {
                        result.Add(ip.toIpAddress().ToString());
                    }
                }
            }
            return result;
        }

        private void LoadConfigFile(string wmiConfigFile) {
            if (wmiConfigFile == null) {
                var dir = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                wmiConfigFile = dir + Path.DirectorySeparatorChar + "default_wmi_config.xml";
            }
            var fileInfo = new FileInfo(wmiConfigFile);
            if (!File.Exists(wmiConfigFile) || (fileInfo.Attributes & FileAttributes.Directory) != 0) {
                throw new ArgumentException("Cannot read file " + wmiConfigFile);
            }
            var xdoc = XDocument.Load(wmiConfigFile);

            var matcher = xdoc.Descendants("matcher");
            if (matcher == null) {
                throw new ArgumentException("\"matcher\" element must be present in the configuration file");
            }
            matchingField = matcher.Select(m => m.Attribute("field").Value).FirstOrDefault();
            if (matchingField == null) {
                throw new ArgumentException("\"matcher\" element must contain the \"field\" attribute");
            }
            matcherFilter = matcher.Select(m => m.Attribute("filter").Value).FirstOrDefault();

            updateOnly = Equals("true", matcher.Select(m => m.Attribute("updateOnly").Value).FirstOrDefault());

            var fields = 
                from field in xdoc.Descendants("field")
                select new {
                    type = field.Attribute("type").Value,
                    assetField = field.Attribute("assetField") != null ? field.Attribute("assetField").Value : null,
                    mod = field.Attribute("mod") != null ? field.Attribute("mod").Value : null,
                    objectClass = field.Attribute("objectClass").Value,
                    format = field.Value,
                    filter = field.Attribute("filter") != null ? field.Attribute("filter").Value : null,
                    merge = field.Attribute("merge") != null ? field.Attribute("merge").Value : null,
                    skipNull = field.Attribute("skipNull") != null ? field.Attribute("skipNull").Value : null,
                    realMac = field.Attribute("realMac") != null ? field.Attribute("realMac").Value : null,
                    valueSeparator = field.Attribute("valueSeparator") != null ? field.Attribute("valueSeparator").Value : ", ",
                    separator = field.Descendants("separator").Any() ? field.Descendants("separator").First().Value : "\n",
                    propertyName = field.Attribute("propertyName") != null ? field.Attribute("propertyName").Value : null,
                    Children = field.Descendants("subField")
                };
            foreach (var field in fields) {
                switch (field.type) {
                    case "single":
                    case "sum":
                        var m = string.IsNullOrEmpty(field.mod) ? 0 : uint.Parse(field.mod);
                        if (field.type == "sum") {
                            config.fields.Add(new ComputeField(field.assetField, field.objectClass, field.propertyName, field.format, m, "add"));
                        } else if (field.type == "single") {
                            config.fields.Add(new SingleField(field.assetField, field.objectClass, field.propertyName, field.format, m, field.valueSeparator));
                        }
                        break;
                    case "multi":
                        var valueSeparator = field.valueSeparator;
                        var nullFieldsToSkip = field.skipNull != null ? field.skipNull.Split(',') : null;
                        var realMacFields = field.realMac != null ? field.realMac.Split(',') : null;
                        var merge = field.merge != null ? int.Parse(field.merge) : -1;
                        var multiField = new MultiField(
                            field.assetField, field.separator, field.objectClass, valueSeparator, 
                            nullFieldsToSkip, realMacFields, field.filter, merge);
                        var subFields =
                            from sub in field.Children
                            select new {
                                label = sub.Attribute("label").Value,
                                propertyName = sub.Attribute("propertyName").Value,
                                format = sub.Value,
                                mod = sub.Attribute("mod") != null ? sub.Attribute("mod").Value : null
                            };
                        foreach (var subField in subFields.Select(
                            sub => {
                                var mod = string.IsNullOrEmpty(sub.mod) ? 0 : uint.Parse(sub.mod);
                                return new SubField(sub.label, sub.propertyName, sub.format, mod);
                            })) {

                            multiField.subFields.Add(subField);
                        }
                        config.fields.Add(multiField);
                        break;
                }
            }
        }
    }
}