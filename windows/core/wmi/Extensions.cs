﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace ephor.scanners.wmi {
    internal static class Extensions {
        public static void SetValueIfNotNull(this Dictionary<string, string> results, string key, string value) {
            if (value == null) {
                return;
            }
            results[key] = value;
        }

        public static uint toInt(this IPAddress address) {
            byte[] bytes = address.GetAddressBytes();
            return BitConverter.ToUInt32(new [] { bytes[3], bytes[2], bytes[1], bytes[0] }, 0);
        }

        public static IPAddress toIpAddress(this uint addr) {
            byte[] bytes = BitConverter.GetBytes(addr);
            return new IPAddress(new[] { bytes[3], bytes[2], bytes[1], bytes[0] });
        }

        public static string ValString(this object val, string separator) {
            if (val.GetType().IsArray) {
                var strings = new List<string>();
                foreach (var v in ((IEnumerable) val)) {
                    strings.Add(v.ToString());    
                }
                return string.Join(separator, strings);
            }
            return val.ToString();
        }

        public static object Mod(this object val, uint modifier = 0) {
            if (val == null) {
                return null;
            }
            if (modifier == 0) {
                return val;
            }
            switch (Type.GetTypeCode(val.GetType())) {
                case TypeCode.Byte:
                    return ((byte)val) / modifier;
                case TypeCode.SByte:
                    return ((SByte)val) / modifier;
                case TypeCode.UInt16:
                    return ((UInt16)val) / modifier;
                case TypeCode.UInt32:
                    return ((UInt32)val) / modifier;
                case TypeCode.UInt64:
                    return ((UInt64)val) / modifier;
                case TypeCode.Int16:
                    return ((Int16)val) / modifier;
                case TypeCode.Int32:
                    return ((Int32)val) / modifier;
                case TypeCode.Int64:
                    return ((Int64)val) / modifier;
                case TypeCode.Decimal:
                    return ((decimal)val) / modifier;
                case TypeCode.Double:
                    return ((double)val) / modifier;
                case TypeCode.Single:
                    return ((Single)val) / modifier;
                default:
                    return val;
            }
        }
    }
}
