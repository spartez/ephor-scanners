﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;

namespace ephor.scanners.net
{
    public class EphorItemUpdater {
        private readonly Uri ephorUri;
        private readonly string jiraUserName;
        private readonly string jiraPassword;
        private string jiraSecurityToken;
        private readonly IList<string> categoryPath;
        private readonly string itemType;
        private readonly string matchingQuery;
        private readonly bool updateOnly;
        private readonly IDictionary<string, string> scanResults;
        private readonly bool verbose;
        private readonly Action<string> verboseOutput;

        public class UpdaterException : Exception {
            public string token { get; private set; }

            public UpdaterException(string message, string token) : base(message) {
                this.token = token;
            }
        }

        public EphorItemUpdater(
            Uri ephorUri,
            string jiraUserName,
            string jiraPassword,
            string jiraSecurityToken,
            IList<string> categoryPath,
            string itemType,
            string matchingQuery,
            bool updateOnly,
            IDictionary<string, string> scanResults,
            bool verbose,
            Action<string> verboseOutput) {

            this.ephorUri = ephorUri;
            this.jiraUserName = jiraUserName;
            this.jiraPassword = jiraPassword;
            this.jiraSecurityToken = jiraSecurityToken;
            this.categoryPath = categoryPath;
            this.itemType = itemType;
            this.matchingQuery = matchingQuery;
            this.updateOnly = updateOnly;

            this.scanResults = mergeResults(scanResults);
            this.verbose = verbose;
            this.verboseOutput = verboseOutput;
        }

        public int Update(out bool updated, out string newToken) {
            if (verbose) {
                verboseOutput.Invoke("updating, matching query: " + matchingQuery);
            }
            Uri uri = BuildUrl(ephorUri, "/rest/com-spartez-ephor/1.0/item/addorupdate", categoryPath, itemType, matchingQuery, updateOnly, jiraUserName, jiraSecurityToken);
            JContainer json = PostJson(uri, jiraUserName, jiraPassword, CreatePayload(scanResults), HttpStatusCode.OK);
            int result = -1;
            updated = false;
            if (jiraUserName == null && jiraSecurityToken != null) {
                newToken = json["token"].Value<string>();
                verboseOutput.Invoke("Received new token " + newToken);
                json = json["items"].Value<JContainer>();
            } else {
                newToken = null;
            }

            if (json is JArray) {
                JArray a = json as JArray;
                if (a.Count > 0) {
                    result = a[0]["id"].Value<int>();
                    updated = true;
                }
            } else {
                result = json["id"].Value<int>();
            }
            return result;            
        }

        private IDictionary<string, string> mergeResults(IDictionary<string, string> input) {
            Dictionary<string, string> results = new Dictionary<string, string>();
            Dictionary<string, IOrderedDictionary> toMerge = new Dictionary<string, IOrderedDictionary>();
            foreach (var key in input.Keys) {
                if (key.StartsWith("--merge--.")) {
                    var weightPos = key.IndexOf("--merge--.", StringComparison.Ordinal) + "--merge--.".Length;
                    var endMarkerPos = key.IndexOf(".==.", StringComparison.Ordinal);
                    var weight = key.Substring(weightPos, endMarkerPos - weightPos);
                    var field = key.Substring(endMarkerPos +".==.".Length);
                    if (!toMerge.ContainsKey(field)) {
                        toMerge[field] = new OrderedDictionary();
                    }
                    toMerge[field].Add(int.Parse(weight), input[key]);
                } else {
                    results[key] = input[key];
                }
            }
            foreach (var key in toMerge.Keys) {
                var sb = new StringBuilder();
                foreach (var k in toMerge[key].Keys) {
                    sb.Append(toMerge[key][k]);
                }
                results[key] = sb.ToString();
            }
            return results;
        }

        private static object CreatePayload(IDictionary<string, string> scanResults) {
            var result = new List<object>();
            foreach (var key in scanResults.Keys) {
                result.Add(
                    new Dictionary<string, object> {
                        {"typeName", key},
                        {"values", new List<string>(new[] {scanResults[key]})}
                    }
                );
            }
            return result;
        }

        private Uri BuildUrl(
            Uri baseUrl, 
            string restEndpoint, 
            IEnumerable<string> categoryPath, 
            string itemType, 
            string matchingQuery, 
            bool updateOnly, 
            string user, 
            string token) {

            var url =
                trimLastSlash(baseUrl) +
                restEndpoint +
                "?category=" + Uri.EscapeDataString(string.Join("/", categoryPath)) +
                "&matchingQuery=" + Uri.EscapeDataString(matchingQuery) +
                "&type=" + itemType +
                "&unique=true"
            ;
            if (updateOnly) {
                url += "&updateOnly=true";
            }
            if (user == null && token != null) {
                url += "&token=" + token;
            }
            verboseOutput.Invoke("Posting to " + url);

            return new Uri(url);
        }

        private static string trimLastSlash(Uri url) {
            string u = url.ToString();
            while (u.EndsWith("/")) {
                u = u.Substring(0, u.Length - 1);
            }
            return u;
        }

        public static JContainer PostJson(Uri url, string user, string password, object json, HttpStatusCode expectedCode) {
            return JsonOp("POST", url, json, expectedCode, user, password);
        }

        private static JContainer JsonOp(string operation, Uri url, object json, HttpStatusCode expectedCode, string user, string password) {
            var codes = new List<HttpStatusCode> { expectedCode };
            return JsonOp(operation, url, json, codes, user, password);
        }

        private static JContainer JsonOp(string operation, Uri url, object json, ICollection<HttpStatusCode> expectedCodes, string user, string password) {
            string val = StringOp(operation, url, json, expectedCodes, user, password);
            var serializerSettings = new JsonSerializerSettings { DateParseHandling = DateParseHandling.None };
            return JsonConvert.DeserializeObject(val, serializerSettings) as JContainer;
        }

        private static string StringOp(string operation, Uri url, object json, ICollection<HttpStatusCode> expectedCodes, string user, string password) {
            try {
                var req = WebRequest.Create(url);

                if (user != null) {
                    SetBasicAuthHeader(req, user, password);
                }

                req.ContentType = "application/json";
                req.Method = operation;
                if (!operation.Equals("GET") && (json != null)) {
                    using (var requestStream = req.GetRequestStream()) {
                        var sw = new StreamWriter(requestStream);
                        var value = JsonConvert.SerializeObject(json);
                        sw.Write(value);
                        sw.Flush();
                        sw.Close();
                    }
                }

                HttpWebResponse response = (HttpWebResponse) req.GetResponse();

                if (expectedCodes.Contains(response.StatusCode)) {
                    using (var stream = response.GetResponseStream()) {
                        var reader = new StreamReader(stream);
                        var value = reader.ReadToEnd();
                        return value;
                    }
                }

                throw new Exception("Unexpected response code " + response.StatusCode + ", expected " + string.Join(", ", expectedCodes));
            } catch (WebException e) {
                var webResponse = e.Response;
                if (webResponse == null) {
                    throw new Exception("Unable to connect to URL: " + url + ", " + e.Message);
                }

                HttpWebResponse httpWebResponse = e.Response as HttpWebResponse;
                if (httpWebResponse != null) {
                    Stream stream = httpWebResponse.GetResponseStream();
                    string jsonMessage = (stream != null) ? new StreamReader(httpWebResponse.GetResponseStream()).ReadToEnd() : "";
                    String message = parseErrorJsonMessage(jsonMessage);

                    if (String.IsNullOrEmpty(message)) {
                        message = e.Status.ToString();
                    }

                    switch (httpWebResponse.StatusCode) {
                        case HttpStatusCode.Unauthorized:
                            throw new UpdaterException("Unauthorized: " + message, getErrorToken(jsonMessage));
                        case HttpStatusCode.Forbidden:
                            throw new UpdaterException("Forbidden: " + message, getErrorToken(jsonMessage));
                        case HttpStatusCode.NotFound:
                            throw new UpdaterException("Not found: " + message, getErrorToken(jsonMessage));
                        case HttpStatusCode.NotAcceptable:
                            throw new UpdaterException("Not acceptable: " + message, getErrorToken(jsonMessage));
                        default:
                            throw new UpdaterException(message, getErrorToken(jsonMessage));
                    }
                }

                throw;
            }
        }

        private static string parseErrorJsonMessage(string jsonMessage) {
            string message = "";
            try {
                var result = JsonConvert.DeserializeObject(jsonMessage) as JContainer;
                var errorMessages = (result != null) ? result["errorMessages"] : null;
                var errors = (result != null) ? result["errors"] : null;
                if (errorMessages != null && errorMessages.HasValues) {
                    message = "Error messages: " + errorMessages;
                }

                if (errors != null && errors.HasValues) {
                    message += String.IsNullOrEmpty(message) ? "" : ". ";
                    message += "Errors: " + errors;
                }

                if (errors == null && errorMessages == null) {
                    message = "Received error: "+ (result != null && result["message"] != null ? result["message"].ToString() : jsonMessage);
                }
                
            } catch (Exception) {
                return jsonMessage;
            }

            return message;
        }

        private static string getErrorToken(string jsonMessage) {
            try {
                var result = JsonConvert.DeserializeObject(jsonMessage) as JContainer;
                var token = (result != null) ? result["token"] : null;
                return token != null ? token.Value<string>() : null;
            } catch(Exception) {
                return null;
            }
        }

        public static void SetBasicAuthHeader(WebRequest req, string u, string p) {
            if (u == null || p == null) {
                return;
            }
            var authInfo = u + ":" + p;
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            req.Headers["Authorization"] = "Basic " + authInfo;
        }
    }
}