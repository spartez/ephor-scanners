﻿using System;
using System.Collections.Generic;

namespace ephor.scanners.api {
    public class Scanners {
        public Scanners() {
        }

        public ICollection<Scanner> GetAvailableScanners() {
            return scanners.Values;
        }

        public void RegisterScanner(Scanner scanner) {
            if (scanners.ContainsKey(scanner.Name)) {
                throw new ArgumentException("scanner with name " + scanner.Name + " already registered");
            }
            scanners.Add(scanner.Name, scanner);
        }

        private readonly Dictionary<string, Scanner> scanners = new Dictionary<string, Scanner>();
    }
}