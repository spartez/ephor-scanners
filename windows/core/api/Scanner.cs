﻿using System;
using System.Collections.Generic;

namespace ephor.scanners.api {
    public interface Scanner {
        string Name { get; }

        void Scan(
            Func<string, string, Dictionary<string, string>, bool, bool> scanCallback, 
            Action<string, Exception> errorCallback, 
            Action<string> progressCallback
        );
    }
}