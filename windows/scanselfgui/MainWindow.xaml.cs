﻿using ephor.scanners.scanselfgui.Properties;
using System;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using ephor.scanners.api;
using ephor.scanners.wmi;
using ephor.scanners.net;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Threading;
using Microsoft.Win32;

namespace ephor.scanners.scanselfgui {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        private void Button_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {

        }

        private void Window_Initialized(object sender, EventArgs e) {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.FileVersion;
            labelVersion.Content = fvi.LegalCopyright + " " + fvi.CompanyName + ". Version " + version;

            textPassword.Password = string.IsNullOrEmpty(Settings.Default.Password) ? "" : decrypt(Settings.Default.Password);
            textConfigFile.Text = String.IsNullOrEmpty(Settings.Default.ConfigFile) ? "Default" : Settings.Default.ConfigFile;
            textFolder.Text = String.IsNullOrEmpty(Settings.Default.Folder) ? "Electronics/Computers" : Settings.Default.Folder;
            textAssetType.Text = String.IsNullOrEmpty(Settings.Default.AssetType) ? "type.computer" : Settings.Default.AssetType;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
            Settings.Default.Password = string.IsNullOrEmpty(textPassword.Password) ? "" : encrypt(textPassword.Password);
            Settings.Default.ConfigFile = "Default".Equals(textConfigFile.Text) ? "" : textConfigFile.Text;
        }

        private string encrypt(string plainText) {
            if (plainText == null) throw new ArgumentNullException("plainText");

            //encrypt data
            var data = Encoding.Unicode.GetBytes(plainText);
            byte[] encrypted = ProtectedData.Protect(data, null, DataProtectionScope.CurrentUser);

            //return as base64 string
            return Convert.ToBase64String(encrypted);
        }

        private string decrypt(string cipher) {
            if (cipher == null) throw new ArgumentNullException("cipher");

            //parse base64 string
            byte[] data = Convert.FromBase64String(cipher);

            //decrypt data
            byte[] decrypted = ProtectedData.Unprotect(data, null, DataProtectionScope.CurrentUser);
            return Encoding.Unicode.GetString(decrypted);
        }

        private void Button_Click(object sender, RoutedEventArgs e) {
            setAllEnabled(false);
            var configFile = "Default".Equals(textConfigFile.Text) ? null : textConfigFile.Text;
            var ephorUri = textJiraUrl.Text;
            var jiraUserName = textJiraUser.Text;
            var jiraPassword = textPassword.Password;
            string categoryPath = textFolder.Text;
            string itemType = textAssetType.Text;

            textLog.Text = "";

            var thread = new Thread(() => {
                var scanners = new Scanners();

                scanners.RegisterScanner(
                    new WmiScanner(
                        configFile,
                        "",
                        "",
                        "127.0.0.1",
                        "0",
                        false,
                        "0",
                        true
                    ));

                foreach (var scanner in scanners.GetAvailableScanners()) {
                    scanAndUpdate(scanner, ephorUri, jiraUserName, jiraPassword, categoryPath, itemType);
                }
            });
            thread.Start();
        }

        private void scanAndUpdate(Scanner scanner, string ephorUri, string jiraUserName, string jiraPassword, string categoryPath, string itemType) {
            addToLog("Running " + scanner.Name + "...");

            string jiraSecurityToken = "";

            scanner.Scan(
                (ip, matchingQuery, scanResult, updateOnly) => {
                    dumpScanResults(ip, scanResult);
                    addToLog("Posting host " + ip + " scan results to " + ephorUri);
                    try
                    {
                        bool updated;
                        int itemId =
                            new EphorItemUpdater(
                                new Uri(ephorUri), jiraUserName, jiraPassword, jiraSecurityToken,
                                categoryPath.Split(new[] { "/" }, StringSplitOptions.None), itemType, matchingQuery, updateOnly, scanResult, true, text => addToLog(text))
                                    .Update(out updated, out jiraSecurityToken);
                        if (updated) {
                            addToLog("Updated asset #" + itemId + " (" + ephorUri + "/plugins/servlet/com.spartez.ephor/item/" + itemId + ")");
                        } else {
                            addToLog("Created asset #" + itemId + " (" + ephorUri + "/plugins/servlet/com.spartez.ephor/item/" + itemId + ")");
                        }
                    }
                    catch (EphorItemUpdater.UpdaterException e) {
                        addToLog("Error posting data to Asset Tracker: " + e.Message);
                    }
                    return true;
                },
                (ip, e) => addToLog("Error scanning host " + ip + ": " + e.Message + dumpException(e)),
                addToLog
            );
            buttonScan.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() => {
                setAllEnabled(true);
            }));
        }

        private string dumpException(Exception e) {
            if (e == null) {
                return "none";
            }
            return "\nmessage: " + e.Message + "\nstack trace: " + e.StackTrace + "\ninner: " + dumpException(e.InnerException);
        }

        private void dumpScanResults(string ip, IDictionary<string, string> scanResult) {
            addToLog("Scan results of host " + ip);
            foreach (var key in scanResult.Keys) {
                addToLog(key + "\n--------------\n" + scanResult[key]);
            }
            addToLog("\n\n");
        }

        private void addToLog(string text) {
            textLog.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() => {
                textLog.Text = textLog.Text + "\n" + text;
                textLog.ScrollToEnd();
                scrollLog.ScrollToBottom();
            }));
        }

        private void setAllEnabled(bool enabled) {
            buttonScan.IsEnabled = enabled;
            buttonChangeConfig.IsEnabled = enabled;
            buttonClearConfig.IsEnabled = enabled;
            textJiraUrl.IsEnabled = enabled;
            textJiraUser.IsEnabled = enabled;
            textPassword.IsEnabled = enabled;
            textFolder.IsEnabled = enabled;
            textAssetType.IsEnabled = enabled;
        }

        private void buttonClearConfig_Click(object sender, RoutedEventArgs e) {
            textConfigFile.Text = "Default";
        }

        private void buttonChangeConfig_Click(object sender, RoutedEventArgs e) {
            var dialog = new OpenFileDialog();
            bool? okClicked = dialog.ShowDialog();
            dialog.Filter = "XML Files (.xml)|*.xml|All Files (*.*)|*.*";
            dialog.FilterIndex = 1;
            if (true == okClicked) {
                textConfigFile.Text = dialog.FileName;
            }
        }
    }
}
