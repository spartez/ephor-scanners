import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


    func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
        return true
    }
    
    @IBAction func openHelp(_ sender: Any) {
        if let url = URL(string: "https://confluence.spartez.com/display/AT4J/SSH+Scanning+for+non-Linux+JIRA"), NSWorkspace.shared().open(url) {
        }
    }
}

