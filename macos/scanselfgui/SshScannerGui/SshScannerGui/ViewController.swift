import Cocoa

class ViewController: NSViewController, NSTextFieldDelegate {

    @IBOutlet weak var jiraUrlText: NSTextField!
    @IBOutlet weak var jiraUserText: NSTextField!
    @IBOutlet weak var jiraPasswordText: NSSecureTextField!
    @IBOutlet weak var configFileLabel: NSTextField!
    @IBOutlet weak var folderText: NSTextField!
    @IBOutlet weak var assetTypeText: NSTextField!
    @IBOutlet var scanLogText: NSTextView!
    @IBOutlet weak var scanButton: NSButton!
    @IBOutlet weak var changeConfigFileButton: NSButton!
    @IBOutlet weak var resetConfigFileButton: NSButton!
    @IBOutlet weak var spinner: NSProgressIndicator!
    
    private var scanning = false
    
    override func loadView() {
        super.loadView()
        jiraUrlText.stringValue = Settings.get().jiraUrl
        jiraUserText.stringValue = Settings.get().jiraUser
        jiraPasswordText.stringValue = Settings.get().jiraPassword
        configFileLabel.stringValue = Settings.get().configFile
        if configFileLabel.stringValue.isEmpty {
            configFileLabel.stringValue = "Default"
        }
        folderText.stringValue = Settings.get().folder
        if folderText.stringValue.isEmpty {
            folderText.stringValue = "Electronics/Computers"
        }
        assetTypeText.stringValue = Settings.get().assetType
        if assetTypeText.stringValue.isEmpty {
            assetTypeText.stringValue = "type.computer"
        }
    }
    
    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    @IBAction func setDefaultConfigFile(_ sender: Any) {
        configFileLabel.stringValue = "Default"
        Settings.get().configFile = ""
    }

    @IBAction func setConfigFile(_ sender: Any) {
        let dialog = NSOpenPanel();
        
        dialog.title                   = "Choose a configuration file";
        dialog.showsResizeIndicator    = true;
        dialog.showsHiddenFiles        = false;
        dialog.canChooseDirectories    = true;
        dialog.canCreateDirectories    = false;
        dialog.allowsMultipleSelection = false;
        dialog.allowedFileTypes        = ["xml"];
        
        if (dialog.runModal() == NSModalResponseOK) {
            let result = dialog.url
            
            if (result != nil) {
                let path = result!.path
                configFileLabel.stringValue = path
                Settings.get().configFile = path
            }
        } else {
            return
        }
    }
    
    @IBAction func scanAndUpdate(_ sender: Any) {
        
        var path = Bundle.main.bundleURL.deletingLastPathComponent().path
        path = "\(path)/Computer Scanner.app/Contents/Resources/scanner"
        let commandLine = "cd \"\(path)\"; ./runscanner.sh scanner.py \(buildCommandLine(censored: false))"
        let commandLineForLog = "cd \"\(path)\"; python scanner.py \(buildCommandLine(censored: true))"
       
        let arguments = ["-c", commandLine]
        
        scanLogText.string = "\nRunning computer scan, \n\n\(commandLineForLog)\n\nplease wait..."
        spinner.startAnimation(self)
        setControlsEnabled(enabled: false)
        runScript(arguments)
        
//        runSomething()
    }
    
    override func controlTextDidChange(_ obj: Notification) {
        let field = obj.object as? NSTextField
        if field == jiraUrlText {
            Settings.get().jiraUrl = jiraUrlText.stringValue.trimmingCharacters(in: .whitespacesAndNewlines)
        } else if field == jiraUserText {
            Settings.get().jiraUser = jiraUserText.stringValue.trimmingCharacters(in: .whitespacesAndNewlines)
        } else if field == jiraPasswordText {
            Settings.get().jiraPassword = jiraPasswordText.stringValue
        } else if field == folderText {
            Settings.get().folder = folderText.stringValue
        } else if field == assetTypeText {
            Settings.get().assetType = assetTypeText.stringValue
        }
    }
    
    private func setControlsEnabled(enabled: Bool) {
        jiraUrlText.isEnabled = enabled
        jiraUserText.isEnabled = enabled
        jiraPasswordText.isEnabled = enabled
        folderText.isEnabled = enabled
        assetTypeText.isEnabled = enabled
        scanButton.isEnabled = enabled
        changeConfigFileButton.isEnabled = enabled
        resetConfigFileButton.isEnabled = enabled
        configFileLabel.textColor = enabled ? NSColor.textColor : NSColor.disabledControlTextColor
    }
    
    private func buildCommandLine(censored: Bool) -> String {
        let assetType = "--asset-type=\(assetTypeText.stringValue)"
        let folder = "--folder='\(folderText.stringValue)'"
        let url = "--jira-url=\(jiraUrlText.stringValue)"
        let user = censored ? "--jira-user=<censored>" : "--jira-user='\(jiraUserText.stringValue)'"
        let password = censored ? "--jira-password=<censored>" : "--jira-password='\(jiraPasswordText.stringValue)'"
        let config = configFileLabel.stringValue == "Default" ? "" : "--xml-config='\(configFileLabel.stringValue)'"
        return "--verbose --self \(assetType) \(folder) \(url) \(user) \(password) \(config)"
    }
    
    private func runScript(_ arguments: [String]) {
        let taskQueue = DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default)
        
        scanning = true
        
        taskQueue.async {
            let path = "/bin/sh"

            let task = Process()
            task.launchPath = path
            task.arguments = arguments
            
            task.terminationHandler = { task in
                DispatchQueue.main.async(execute: {
                    self.setControlsEnabled(enabled: true)
                    self.spinner.stopAnimation(self)
                    self.scanning = false
                })
            }
            
            task.launch()
            task.waitUntilExit()
            
            self.readScanLog()
        }
        
    }

    private func readScanLog() {
        let tempDirectoryTemplate = NSTemporaryDirectory()
        let logFile = "\(tempDirectoryTemplate)asset-tracker-scanner-log.txt"
        
        do {
            let text = try String(contentsOf: NSURL(fileURLWithPath: logFile) as URL, encoding: String.Encoding.utf8)
            appendToLog(text: text)
        }
        catch {
            appendToLog(text: "Error reading log file: \(error)")
        }
    }
    
    private func appendToLog(text: String) {
        DispatchQueue.main.async(execute: {
            let previousOutput = self.scanLogText.string ?? ""
            let nextOutput = previousOutput + "\n" + text
            self.scanLogText.string = nextOutput
            self.scanLogText.scrollToEndOfDocument(self)
        })
    }
}


