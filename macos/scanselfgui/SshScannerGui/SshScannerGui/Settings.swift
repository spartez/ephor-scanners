import Foundation

class Settings {
    private static var instance: Settings? = nil
    
    //MARK: Initialization
    
    class func get() -> Settings {
        if instance == nil {
            instance = Settings()
        }
        return instance!
    }
    
    init() {
        UserDefaults.standard.synchronize()
    }
    
    var jiraUrl: String {
        get {
            var val = UserDefaults.standard.value(forKey: "jiraUrl")
            if val == nil {
                val = ""
            }
            return val as! String
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "jiraUrl")
            UserDefaults.standard.synchronize()
        }
    }

    
    var jiraUser: String {
        get {
            var val = UserDefaults.standard.value(forKey: "jiraUser")
            if val == nil {
                val = ""
            }
            return val as! String
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "jiraUser")
            UserDefaults.standard.synchronize()
       }
    }

    
    var jiraPassword: String {
        get {
            var val = UserDefaults.standard.value(forKey: "jiraPassword")
            if val == nil {
                val = ""
            }
            return val as! String
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "jiraPassword")
            UserDefaults.standard.synchronize()
        }
    }

    
    var configFile: String {
        get {
            var val = UserDefaults.standard.value(forKey: "configFile")
            if val == nil {
                val = ""
            }
            return val as! String
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "configFile")
            UserDefaults.standard.synchronize()
        }
    }

    
    var folder: String {
        get {
            var val = UserDefaults.standard.value(forKey: "folder")
            if val == nil {
                val = ""
            }
            return val as! String
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "folder")
            UserDefaults.standard.synchronize()
        }
    }

    
    var assetType: String {
        get {
            var val = UserDefaults.standard.value(forKey: "assetType")
            if val == nil {
                val = ""
            }
            return val as! String
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "assetType")
            UserDefaults.standard.synchronize()
        }
    }
}
