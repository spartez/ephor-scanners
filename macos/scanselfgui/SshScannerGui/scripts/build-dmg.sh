#!/bin/sh

if [ "${CONFIGURATION}" = "Release" ]; then
    echo "building DMG"
    cd "${TARGET_BUILD_DIR}"
    rm -f *.dmg
    create-dmg "Computer Scanner.app"
    mkdir -p "${PROJECT_DIR}"/build/dmg
    rm -rf "${PROJECT_DIR}"/build/dmg/*.dmg
    mv *.dmg "${PROJECT_DIR}"/build/dmg/
    open "${PROJECT_DIR}"/build/dmg/
fi


