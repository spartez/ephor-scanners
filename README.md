This project contains scanners that read information about computers on the network and report it to Asset Tracker for JIRA.
Licensed under Apache license (see [LICENSE.txt](LICENSE.txt)). Feel free to clone and modify to your liking.

## Windows - WMI Scanner

It is a commandline app that scans Windows computers on the network using WMI and reports to Asset Tracker.

It can be downloaded from the [downloads page](https://bitbucket.org/spartez/ephor-scanners/downloads/). 

Latest version is [1.2.0](https://bitbucket.org/spartez/ephor-scanners/downloads/asset-tracker-wmi-scanner-windows-1.2.0.msi)

It requires [.Net Framework 4.7.2](https://dotnet.microsoft.com/download/dotnet-framework-runtime)

## Linux and macOS - SSH Scanner

It is a commandline app that scans Linux and macOS computers on the network using SSH and reports to Asset Tracker.

It can be downloaded from the [downloads page](https://bitbucket.org/spartez/ephor-scanners/downloads/) and must be installed on Linux or macOS.

Latest version is 1.0.2

* [tar archive - Linux and macOS](https://bitbucket.org/spartez/ephor-scanners/downloads/asset-tracker-ssh-scanner-linux-and-macos_1.0.2-20180508131757.tar.gz)
* [DEB package](https://bitbucket.org/spartez/ephor-scanners/downloads/asset-tracker-ssh-scanner-linux_1.0.2-20180508131757.deb)
* [RPM package](https://bitbucket.org/spartez/ephor-scanners/downloads/asset-tracker-ssh-scanner-linux-1.0.2-20180508131757.noarch.rpm)

Mac users can use a GUI app for scanning their computer (not the network range). App is [available here](https://bitbucket.org/spartez/ephor-scanners/downloads/Computer-Scanner-1.0.2.dmg)

## Android - Label Scanner

An app that can be used to quickly scan labels generated from Asset Tracker and set a specified date field on the asset record to "now" or execute a [stored operation sequence](https://confluence.spartez.com/display/AT4J/Invoking+operation+sequences+by+scanning+asset+label). It can be installed from [Google Play Store](https://play.google.com/store/apps/details?id=com.spartez.assettracker.checkin)

## iOS - Label Scanner

An app that can be used to quickly scan labels generated from Asset Tracker and set a specified date field on the asset record to "now" or execute a [stored operation sequence](https://confluence.spartez.com/display/AT4J/Invoking+operation+sequences+by+scanning+asset+label). Under construction