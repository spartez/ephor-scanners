# Scanner

## How to test

1. Generate a self-signed TLS certificate with **your own CA** (Step 1B from [this page](https://web.archive.org/web/20160403100211/https://metabrot.rocho.org/jan/selfsign.html), don't skip any password because Jira config will require one)
1. Run your `locahost` Jira and configure it to run on https with the self-signed  [TLS certificate](https://confluence.atlassian.com/adminjiraserver/running-jira-applications-over-ssl-or-https-938847764.html)
1. Run a *nix VM connected to the virtual interface so you can ping it from your OS
1. Run the scanner using **Python 2.7**:

    ```bash
    $ python2.7 ./unix/scanner/src/scanner.py  \
      --ssh-user=debone                        \
      --ping-first                             \
      --ping-timeout=10                        \
      --range=192.168.99.102-192.168.99.103    \
      --asset-type=type.computer               \
      --folder=Electronics                     \
      --jira-user=admin                        \
      --jira-password=admin                    \
      --jira-url=https://localhost:8080        \
      --cacert=<path>                          \
      --verbose                                
    ```