%define        __spec_install_post %{nil}
%define          debug_package %{nil}
%define        __os_install_post %{_dbpath}/brp-compress

Summary: Asset Tracker Scanner
Name: asset-tracker-scanner
Version: VERSION
Release: TIMESTAMP
License: Apache
SOURCE0 : %{name}_%{version}-TIMESTAMP.tar.gz
BuildArch: noarch
Requires: python >= 2.6.0, curl, openssh-clients, sshpass

BuildRoot: %{_tmppath}/%{name}_%{version}-%{release}-root

%description
Scans computers on specified subnets and reports them to Asset Tracker for JIRA

%prep
%setup -q

%build
#

%install
rm -rf %{buildroot}
mkdir -p  %{buildroot}

# in builddir
cp -a * %{buildroot}


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
/opt/spartez/asset-tracker-scanner/

