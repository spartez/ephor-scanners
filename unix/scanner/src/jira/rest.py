import json
import os
import subprocess
import tempfile
import urllib

import sys

from config import ConfigException


class AssetTrackerUpdate:

    def __init__(self, args, matcher, token):
        self.baseUrl = args['jira-url'].val
        self.user = args['jira-user'].val
        self.token = token
        self.password = args['jira-password'].val
        self.folder = args['folder'].val
        self.assetType = args['asset-type'].val
        self.cacert = args['cacert'].val
        if self.password is None:
            self.password = ''
        self.verbose = args['verbose'].val
        self.matcher = matcher
        if self.baseUrl is None:
            raise ConfigException('--jira-url parameter is required')
        if self.user is None and self.token is None:
            raise ConfigException('--jira-user or --jira-token parameter is required')
        if self.assetType is None:
            raise ConfigException('--asset-type parameter is required')
        if self.folder is None:
            raise ConfigException('--folder parameter is required')
        if self.matcher is None:
            raise ConfigException('matcher is required')

    def update(self, macs, updateJson):
        with tempfile.NamedTemporaryFile(delete=False) as tmp:
            self.__saveJsonToFile(updateJson, tmp)
            command = self.__buildCurlCommand(macs, tmp)
            if self.verbose:
                if self.token is not None:
                    print 'Running curl command: %s' % ' '.join(command).replace(self.token, '<redacted>')
                elif self.user is not None:
                    print 'Running curl command: %s' % ' '.join(command).replace(self.user + ':' + self.password, '<redacted>:<redacted>')
            else:
                print 'Running curl to update asset record...'
            p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = p.communicate()
            os.remove(tmp.name)

            try:
                asset = json.loads(out)
                if self.token is not None:
                    self.token = asset['token']
                    try:
                        asset = asset['items']
                    except:
                        if asset.has_key('message'):
                            message = asset['message']
                            print 'Error updating asset data: %s' % message
                        return self.token
                else:
                    if not isinstance(asset, list) and asset.has_key('message'):
                        message = asset['message']
                        print 'Error updating asset data: %s' % message

                if isinstance(asset, list):
                    print 'Updated asset record: %s/plugins/servlet/com.spartez.ephor/item/%d' % (self.baseUrl, asset[0]['id'])
                else:
                    if asset.has_key('id'):
                        print 'Created asset record: %s/plugins/servlet/com.spartez.ephor/item/%d' % (self.baseUrl, asset['id'])
                return self.token
            except (ValueError):
                if self.verbose:
                    print 'Error parsing Asset Tracker response\n%s' % out
                else:
                    print 'Error parsing Asset Tracker response (try with --verbose flag)'
            return None

    def __saveJsonToFile(self, data, file):
        json.dump(data, file)
        file.close()

    def __buildCurlCommand(self, macs, updateFile):
        command = [
            'curl',
            '--data',
            '@' + updateFile.name,
            '-H',
            'Content-Type: application/json'
        ]
        
        if self.cacert is not None:
            command = command + ['--cacert', self.cacert]
            
        if self.token is None and self.user is not None:
            command = command + ['-u', self.user + ':' + self.password]

        query = 'category=' + urllib.quote(self.folder) + self.__buildMatchingQuery(macs) + '&type=' + urllib.quote(self.assetType) + '&unique=true'
        if self.matcher.attrib.has_key('updateOnly'):
            query += '&updateOnly=' + self.matcher.attrib['updateOnly']
        if self.token is not None:
            query += '&token='+ self.token
        
        command.append(self.baseUrl + '/rest/com-spartez-ephor/1.0/item/addorupdate?' + query)

        return command

    def __buildMatchingQuery(self, macs):
        field = self.matcher.attrib['field']
        result = []
        for mac in macs:
            result.append(field + '=' + mac)
        if len(result) == 0:
            return ''
        return '&matchingQuery=' + urllib.quote(' OR '.join(result))