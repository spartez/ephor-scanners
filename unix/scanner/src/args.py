import collections


class Arg:
    def __init__(self, hasVal, spaces, mandatory, description, order):
        self.mandatory = mandatory
        self.hasVal = hasVal
        self.val = None if hasVal else False
        self.description = description
        self.spaces = spaces
        self.order = order

class Args:
    argv = None
    args = {
        'usage': Arg(False, '       ', False, 'Print usage', 0),
        'verbose': Arg(False, '     ', False, 'Verbose scan and update operations', 1),
        'range': Arg(True, '        ', False, 'Address range of hosts to scan. Aither this or --self must be provided', 2),
        'self': Arg(False, '        ', False, 'Scan this computer only. Either this or --range must be provided', 3),
        'ssh-user': Arg(True, '     ', False, 'User name to connector with SSH. Either this parameter, or --ssh-key-file must be provided', 4),
        'ssh-password': Arg(True, ' ', False, 'Password to connect with SSH', 5),
        'ssh-key-file': Arg(True, ' ', False, 'Identity file (private key) to use to connect with SSH. Either this parameter, or --ssh-user must be provided', 6),
        'xml-config': Arg(True, '   ', False, 'XML configuration file containing scanner parameters. If not provided, default file is used', 7),
        'ping-first': Arg(False, '  ', False, 'Ping host before scanning with SSH', 8),
        'ping-timeout': Arg(True, ' ', False, 'Ping timeout (in seconds)', 9),
        'folder': Arg(True, '       ', True, 'Folder path to place newly created asset records in', 10),
        'asset-type': Arg(True, '   ', True, 'Type of asset to create', 11),
        'jira-url': Arg(True, '     ', True, 'Your JIRA URL', 12),
        'jira-user': Arg(True, '    ', False, 'JIRA username for updating asset record. Either it, or --jira-token must be provided', 13),
        'jira-password': Arg(True, '', False, 'JIRA password for updating asset record. Either it, or --jira-token must be provided', 14),
        'jira-token': Arg(True, '   ', False, 'JIRA security token for updating asset record. Either it, or --jira-user and --jira-password must be provided', 15),
        'version': Arg(False, '     ', False, 'Print program version', 16),
        'cacert': Arg(True, '       ', False, 'Path to your root CA certificate in case your Jira instance uses a self-signed TLS certificate', 17)
    }

    def __init__(self, argv):
        self.argv = argv
        for i in range(0, len(argv)):
            if argv[i].startswith('--'):
                pair = argv[i].split('=')
                name = pair[0][2:]
                if name in self.args:
                    if self.args[name].hasVal:
                        self.args[name].val = pair[1]
                    else:
                        self.args[name].val = True

    def __getitem__(self, item):
        if item in self.args:
            return self.args[item]
        return None

    def usage(self):
        print 'Usage: python %s [options]' % self.argv[0]
        print 'Options:\n'
        args = []
        for arg in self.args:
            args.append({
                'order': self.args[arg].order,
                'name': arg, 'val': self.args[arg]
            })
        ordered = sorted(args, key=lambda a: a['order'])
        for el in ordered:
            name = '--' + el['name'] + '=<value>' if el['val'].hasVal else '--' + el['name']
            spaces = el['val'].spaces + ('         ' if not el['val'].hasVal else '')
            mandatory = '*' if el['val'].mandatory else ' '
            print '\t%s%s %s %s' % (mandatory, name, spaces, el['val'].description)
        print '\n(*) denote mandatory parameters\n'
