import subprocess

class IcmpPinger:
    def __init__(self, host, args):
        self.host = host
        self.verbose = args['verbose'].val
        timeout = args['ping-timeout'].val
        if timeout is not None:
            self.timeout = timeout
        else:
            self.timeout = 10

    def ping(self):
        print 'Pinging host %s...' % self.host
        p = subprocess.Popen(['ping', '-W', '%s' % self.timeout, '-q', '-c', '1', self.host], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()
        if self.verbose:
            if len(out) > 0:
                print out
            if len(err) > 0:
                print err
        return p.returncode == 0
