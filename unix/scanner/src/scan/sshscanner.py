import shlex
import subprocess

from config import ConfigException


class Results:
    def __init__(self):
        self.res = {}

    def add(self, field, value):
        key = field.attrib['assetField']
        format = '%s'
        if field.attrib.has_key('format'):
            format = field.attrib['format'].replace('\\n', '\n')
        merge = -1
        if field.attrib.has_key('merge'):
            merge = int(field.attrib['merge'])
        val = format % value
        if not self.res.has_key(key):
            self.res[key] = []
        self.res[key].append({ 'merge': merge, 'value': val })

    def toUpdateData(self):
        val = []
        for k in self.res.keys():
            s = sorted(self.res[k], key=lambda el: el['merge'])
            if len(s) > 0:
                if s[0]['merge'] < 0:
                    val.append({ 'typeName': k, 'values': [ s[0]['value']] })
                else:
                    val.append({ 'typeName': k, 'values': [ '\n'.join(map(lambda el: el['value'], s)) ] })
        return val

class SshScanner:
    options = "-oStrictHostKeyChecking=no"

    def __init__(self, host, args):
        self.path = None
        self.verbose = args['verbose'].val
        self.host = host
        self.user = args['ssh-user'].val
        self.password = args['ssh-password'].val
        self.keyFile = args['ssh-key-file'].val
        if self.host is None:
            raise ConfigException('host must be specified')
        if self.host != '127.0.0.1' and self.keyFile is None and self.user is None:
            raise ConfigException('Either --ssh-user and --ssh-password or --ssh-key-file parameters must be specified')

    def identifyOs(self):
        print 'Identifying OS...'
        p = subprocess.Popen(self.__buildSshCommand(['uname ; hostname']), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()
        if p.returncode != 0:
            raise Exception(err)
        if self.verbose:
            print 'Detected OS: %s' % out
        return out.split('\n')

    def scanFields(self, fields, path):
        print 'Command path on scanned host (%s) is: %s' % (self.host, path)
        self.path = path
        procFields = {}
        sysCtlFields = []
        lsHwFields = []
        commandFields = []

        for field in fields:
            if field.attrib['type'] == 'single' and field.attrib['scanner'] == 'proc':
                if not procFields.has_key(field.attrib['path']):
                    procFields[field.attrib['path']] = []
                procFields[field.attrib['path']].append(field)
            if field.attrib['scanner'] == 'lshw':
                lsHwFields.append(field)
            if field.attrib['scanner'] == 'command':
                commandFields.append(field)
            if field.attrib['scanner'] == 'sysctl':
                sysCtlFields.append(field)

        results = Results()

        if len(procFields) > 0:
            self.__scanProcFields(procFields, results)
        if len(sysCtlFields) > 0:
            self.__scanSysCtlFields(sysCtlFields, results)
        if len(lsHwFields) > 0:
            self.__scanLsHwFields(lsHwFields, results)
        if len(commandFields) > 0:
            self.__scanCommandFields(commandFields, results)

        return results

    def scanMacAddresses(self, matcher):
        command = matcher.attrib['command']
        post = None
        if matcher.attrib.has_key('post'):
            post = matcher.attrib['post']
        raw = self.__scanCommand(command, True)
        if post is not None:
            postCmd = shlex.split(post)
            post = subprocess.Popen(postCmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
            echo = subprocess.Popen(['echo', raw], stdout=post.stdin)
            out, err = post.communicate()
            echo.wait()
            raw = out.strip()
        macs = raw.split('\n')
        result = []
        for mac in macs:
            if len(mac) < 15 or mac[1].lower() == '2' or mac[1].lower() == '6' or mac[1].lower() == 'a' or mac[1].lower() == 'e':
                continue
            result.append(mac)
        if len(result) == 0:
            raise Exception('Unable to retrieve MAC addresses')
        return result

    def __scanCommand(self, command, errorOnFail):
        if self.verbose:
            print 'Running command: %s' % command
        p = subprocess.Popen(self.__buildSshCommand(shlex.split(command)), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()
        if errorOnFail and p.returncode != 0:
            raise Exception(err)

        if self.verbose:
            print '    Received:\n%s' % out
        return out

    def __scanCommandFields(self, fields, results):
        print 'Scanning using commands...'
        for field in fields:
            command = field.attrib['command']
            errorOnFail = True
            if field.attrib.has_key('onFail') and field.attrib['onFail'] == 'pass':
                errorOnFail = False
            print '\t%s' % command
            val = self.__postProcess(field, self.__scanCommand(command, errorOnFail))
            if field.attrib['type'] == 'single':
                val = val.strip()
            results.add(field, val)

    def __scanLsHwFields(self, fields, results):
        filters = []
        canFail = True
        for f in fields:
            filters.append(f.attrib['filter'])
            if not f.attrib.has_key('onFail') or f.attrib['onFail'] != 'pass':
                canFail = False
        print 'Scanning using lshw (%s)...' % ','.join(filters)
        toFilterOut = [
            'clock', 'width', 'resources', 'capabilities', 'configuration', 'physical', 'pci@', 'logical', 'version'
        ]
        grep = ' | grep -v '
        p = subprocess.Popen(self.__buildSshCommand(shlex.split('lshw' + grep + grep.join(toFilterOut))), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()
        if p.returncode != 0:
            if canFail:
                print 'WARNING: "lshw" command failed (probably not found): %s' % err
                print 'Consider installing "lshw" command on scanned machine to improve scan quality'
            else:
                raise Exception('Required "lshw" command failed (probably not found): ' + err + '\n')
        sections = out.split('*-')
        for field in fields:
            for sec in sections:
                # print sec
                val = sec.strip()
                filter = field.attrib['filter']
                if val.startswith(filter):
                    val = val[len(filter):].strip()
                    if self.verbose:
                        print 'Received:\n%s' % val
                    results.add(field, self.__postProcess(field, val))

    def __scanProcFields(self, fieldsMap, results):
        for path in fieldsMap.keys():
            self.__scanProcPath(path, fieldsMap[path], results)

    def __scanProcPath(self, path, fields, results):
        params = []
        for f in fields:
            if f.attrib.has_key('param'):
                params.append(f.attrib['param'])
        print 'Scanning %s (%s)...' % (path, ','.join(params))
        p = subprocess.Popen(self.__buildSshCommand(['cat', path]), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()
        if p.returncode != 0:
            raise Exception(err)
        lines = out.split('\n')
        if self.verbose:
            print 'Received:\n%s' % out
        for field in fields:
            for l in lines:
                if not field.attrib.has_key('param') or l.startswith(field.attrib['param']):
                    results.add(field, self.__postProcessProc(field, l))

    def __scanSysCtlFields(self, fields, results):
        props = []
        for f in fields:
            props.append(f.attrib['prop'])
        print 'Scanning using sysctl (%s)...' % ','.join(props)
        p = subprocess.Popen(self.__buildSshCommand(shlex.split('sysctl -a')), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()
        if p.returncode != 0:
            raise Exception('Required "sysctl" command failed: ' + err + '\n')
        entries = out.split('\n')
        for field in fields:
            for entry in entries:
                val = entry.strip()
                prop = field.attrib['prop']
                if val.startswith(prop):
                    val = val[len(prop) + 1:].strip()
                    if self.verbose:
                        print 'Received:\n%s' % val
                    results.add(field, self.__postProcess(field, val))

    def __postProcess(self, field, value):
        if not field.attrib.has_key('post'):
            return value
        postCmd = shlex.split(field.attrib['post'])
        post = subprocess.Popen(postCmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        echo = subprocess.Popen(['echo', value], stdout=post.stdin)
        out, err = post.communicate()
        echo.wait()
        return out.strip()

    def __postProcessProc(self, field, value):
        pre = value.strip()
        if field.attrib.has_key('separator'):
            pre = value.split(field.attrib['separator'])[1].strip()
        if not field.attrib.has_key('post'):
            return pre
        postCmd = shlex.split(field.attrib['post'])
        post = subprocess.Popen(postCmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        echo = subprocess.Popen(['echo', pre], stdout=post.stdin)
        out, err = post.communicate()
        echo.wait()
        return out.strip()

    def __buildShellCommand(self, command):
        cmd = [ '/bin/sh', '-c', ]
        logCmd = [ '/bin/sh', '-c']
        cmd.append(' '.join(command))
        logCmd.append(' '.join(command))
        if self.verbose:
            print 'Command line: %s' % logCmd
        return cmd

    def __buildSshCommand(self, command):
        if self.host == '127.0.0.1':
            return self.__buildShellCommand(command)

        cmd = []
        logCmd = []
        if self.__useSshPass() and self.password is not None:
            cmd.append('sshpass')
            cmd.append('-p')
            cmd.append(self.password)
            logCmd.append('sshpass')
            logCmd.append('-p')
            logCmd.append('<redacted>')
        cmd.append('ssh')
        cmd.append(self.options)
        logCmd.append('ssh')
        logCmd.append(self.options)
        if not self.__useSshPass():
            cmd.append('-i')
            cmd.append(self.keyFile)
            logCmd.append('-i')
            logCmd.append(self.keyFile)
            cmd.append('-oBatchMode=yes')
            logCmd.append('-oBatchMode=yes')
        if self.__useSshPass():
            cmd.append(self.user + '@' + self.host)
            logCmd.append('<redacted>@' + self.host)
        else:
            cmd.append(self.host)
            logCmd.append(self.host)

        if self.path is not None:
            cmd.append('PATH=' + self.path)
            cmd.append(';')
            logCmd.append('PATH=' + self.path)
            logCmd.append(';')
        for c in command:
            cmd.append(c)
            logCmd.append(c)

        if self.verbose:
            print 'Command line: %s' % logCmd
        return cmd

    def __useSshPass(self):
        return self.keyFile is None
