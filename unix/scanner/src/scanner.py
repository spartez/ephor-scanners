import sys
import traceback

from args import Args
from scan.pinger import IcmpPinger
from scan.sshscanner import SshScanner
from config import XmlConfig, ConfigException
from hosts import HostSelector
from jira.rest import AssetTrackerUpdate
import version

if __name__ == "__main__":
    args = Args(sys.argv)
    if len(sys.argv) < 2 or args['usage'].val:
        args.usage()
    elif args['version'].val:
        print 'Version: %s' % version.__version__
    else:
        config = XmlConfig(args)
        range = args['range'].val
        verbose = args['verbose'].val
        scanSelf = args['self'].val
        if range is None and not scanSelf:
            print '--range or --self parameter must be provided'
        else:
            if scanSelf:
                print 'Scanning this computer...'
            else:
                print 'Scanning network range %s...' % range
            try:
                host = None
                if scanSelf:
                    host = '127.0.0.1'
                else:
                    hostSelector = HostSelector(range)
                    host = hostSelector.nextHost()
                token = args['jira-token'].val
                while host is not None:
                    try:
                        print '------- Scanning host %s' % host
                        if scanSelf or not args['ping-first'].val or IcmpPinger(host, args).ping():
                            scanner = SshScanner(host, args)
                            os = scanner.identifyOs()
                            osType = os[0].lower()
                            matcher = config.getMatcher(osType)
                            if matcher is None:
                                raise Exception('"matcher" element is required for the host OS type: ' + osType)
                            print 'Retrieving host MAC addresses for asset record matching...'
                            macs = scanner.scanMacAddresses(matcher)
                            print 'Received MACs %s' % macs
                            print 'Scanning host: %s (OS: %s)' % (os[1], osType)
                            fields = config.getFields(osType)
                            path = config.getOsPath(osType)
                            result = scanner.scanFields(fields, path)
                            data = result.toUpdateData()
                            if verbose:
                                print 'Scan results:\n\n%s' % data
                            updater = AssetTrackerUpdate(args, matcher, token)
                            token = updater.update(macs, data)
                        else:
                            print 'Host scan skipped, ping failed'
                    except ConfigException as e:
                        print e
                        args.usage()
                    except:
                        traceback.print_exc(file=sys.stdout)

                    if scanSelf:
                        break
                    host = hostSelector.nextHost()
                print 'Finished scanning'
            except:
                traceback.print_exc(file=sys.stdout)
                print args.usage()
