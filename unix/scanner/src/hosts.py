import ipaddr


class HostSelector:
    def __init__(self, range):
        self.range = []
        self.index = 0

        ranges = range.split(',')
        for r in ranges:
            subr = r.split('-')
            start = ipaddr.IPv4Address(subr[0])
            self.range.append(start.exploded)
            if len(subr) > 1:
                end = ipaddr.IPv4Address(subr[1])
                i = 1
                while True:
                    current = start + i
                    self.range.append(current.exploded)
                    i += 1
                    if current == end:
                        break

    def nextHost(self):
        if self.index >= len(self.range):
            return None
        r = self.range[self.index]
        self.index += 1
        return r
