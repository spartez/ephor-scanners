import sys
from xml.etree import ElementTree


class ConfigException(Exception):
    def __init__(self, message):
        super(Exception, self).__init__(message)


class XmlConfig:
    root = None

    def __init__(self, args):
        configFile = args['xml-config'].val
        if configFile is None:
            path = sys.argv[0]
            if '/' not in path:
                path = ''
            else:
                path = path[:path.rfind('/')] + '/'
            configFile = path + 'default-config.xml'
            print 'Using default config file: %s' % configFile
        else:
            print 'Using config file: %s' % configFile

        self.root = ElementTree.parse(configFile).getroot()

    def getOsPath(self, osType):
        for os in self.root.findall('os'):
            if os.attrib['type'] != osType:
                continue
            return os.attrib['path']

    def getFields(self, osType):
        fields = []
        for os in self.root.findall('os'):
            if os.attrib['type'] != osType:
                continue
            for f in os.findall('field'):
                fields.append(f)
        return fields

    def getMatcher(self, osType):
        for os in self.root.findall('os'):
            if os.attrib['type'] != osType:
                continue
            for matcher in os.findall('matcher'):
                return matcher
        return None
